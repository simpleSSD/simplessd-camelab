This is SimpleSSD-Gem5FS source code.
Usage example:
./build/ARM/gem5.opt --debug-flags=IdeDisk,HIL,FTLOut,PAL2,GLOBALCONFIG --debug-file=SimpleSSD.txt ./configs/example/fs.py --num-cpu=1 --dtb-filename=XXX --disk-image=XXX --kernel=XXX --machine-type=XXX --script=XXX --SSD=1 --SSDconfig=./sample_cfg/rev_CH8_MLC.cfg