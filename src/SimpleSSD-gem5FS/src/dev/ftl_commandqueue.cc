//
//  CommandQueue.cpp
//  FTLSim_functional
//
//  Created by Narges on 6/26/15.
//  Copyright (c) 2015 narges shahidi. All rights reserved.
//

#include "dev/ftl_commandqueue.hh"
//#include "debug/FTLOut.hh"

void Command::to_string(int disk){
	
	switch (operation) {
	case OP_READ:
		DPRINTF(FTLOut, "FTL: disk[%d] %lld Read %lld \n", disk, curTick, page_address);

		break;
	case OP_WRITE:
		DPRINTF(FTLOut, "FTL: disk[%d] %lld Write %lld \n", disk, curTick, page_address);
		break;
	case OP_ERASE:
		DPRINTF(FTLOut, "FTL: disk[%d] %lld Erase %lld \n", disk, curTick, page_address);
		break;
	default:
		break;
	}

}
void Command::to_string(std::ofstream &file, int disk){

	switch (operation) {
	case OP_READ:

		DPRINTF(FTLOut,"FTL: disk[%d] %lld Read %lld \n", disk, curTick, page_address);

		break;
	case OP_WRITE:
		DPRINTF(FTLOut, "FTL: disk[%d] %lld Write %lld \n", disk, curTick, page_address);
		break;
	case OP_ERASE:
		DPRINTF(FTLOut,"FTL: disk[%d] %lld Erase %lld \n", disk, curTick, page_address);
		break;
	default:
		break;
	}

}



// ===========================================================


CommandQueue::CommandQueue(FTL * f){
    command_count = 0;
    queue_head = NULL;
    queue_tail = NULL;
	ftl = f; 
	
	ResetStats(); 
}
CommandQueue::~CommandQueue(){
//	que_file.close(); 
}
Command * CommandQueue::popFromQueue(){
	
    if (queue_head == NULL) return NULL;
    Command * com = queue_head;
	 
    queue_head = queue_head->next_command;
    command_count--;
    return com;
	

}
void CommandQueue::flushQueue(){
	Command * prev_com = NULL; 
	Command * com = getFromQueue(0); 	
	while (com) {
	
		if (com->synced){ 
			ftl->PAL_SetLatency(com->seq_num, com->finished_time - com->arrived_time + 1); 
		}
		
		if (!com->initial){
	
			if (com->finished_time != 0 && com->finished_time <= ftl->current_time){
				updateStats(com);
	
				removeCommand(&prev_com, &com); // com is going to be the next command after calling this 

			}else {
				prev_com = com; 
				com = com->next_command; 
			}
	
		}else {
			if (com->synced) 
				ftl->PAL_SetLatency(com->seq_num, com->finished_time - com->arrived_time + 1); 
		

			removeCommand(&prev_com, &com); 
		}
		

	}
}

void CommandQueue::removeCommand(Command ** prev, Command ** com){

	command_count--; 	
	if ((*com) == queue_tail){
		queue_tail = (*prev);
		if ((*prev) != NULL) { 
			(*prev)->next_command = NULL;
			
		} else {
			queue_head = NULL; 
		}
		delete (*com); 
		(*com) = NULL; 
		return;  
	}
		
	if ((*com) == queue_head){
		queue_head = (*com)->next_command; 
		delete (*com);
		(*com) = queue_head;  
		return; 
	}
	
	(*prev)->next_command = (*com)->next_command; 
	delete (*com); 
	(*com) = (*prev)->next_command; 
}
int CommandQueue::pushToQueue(long int seq_num, Tick time, Addr address, int type, bool synced, bool init = false){

	
	if (address == -1){
		assert("wrong command pushed to the queue"); 
	}
	Command * com = new Command(seq_num, time, address, type, synced, init);


           
    if (queue_head == NULL){
        queue_head = com;
        queue_tail = com;
    }else {
        queue_tail->next_command = com;
        queue_tail = com;
    }
    
    command_count++;
	
    return command_count-1;

}
Command * CommandQueue::getFromQueue(long int index){
	
    if (index >= command_count) return NULL;
    Command * com = queue_head;
    while (index != 0) {
        com = com->next_command;
        index--;
    }
    return com;
	 
}

void CommandQueue::addToTail(Command * com){
    if (queue_head == NULL){
        queue_head = com;
        queue_tail = com;
    }else {
        queue_tail->next_command = com;
        queue_tail = com;
    }
    
    command_count++;
	
}
void CommandQueue::ResetStats(){
    // resetting Statistics
	pal_read_req_count = 0; 
	pal_write_req_count = 0; 
	pal_erase_req_count = 0; 

	pal_read_capacity = 0; // sector
	pal_write_capacity = 0; // sector 

	pal_read_lat_min = DBL_MAX;
	pal_read_lat_max = 0; 
	pal_read_lat_avg = 0; 
	
	pal_write_lat_min = DBL_MAX;  
	pal_write_lat_max = 0; 
	pal_write_lat_avg = 0; 
	
	pal_erase_lat_min = DBL_MAX; 
	pal_erase_lat_max = 0; 
	pal_erase_lat_avg = 0; 	
}

void CommandQueue::PrintStats(){
    // printing Statistics

	flushQueue(); 	

}

void CommandQueue::updateStats(Command * com){
	
	Tick command_latency = (com->finished_time - com->arrived_time + 1) / USEC; // convert ps to us  

	switch (com->operation) {
		case OP_READ:
			if (command_latency < pal_read_lat_min)
				pal_read_lat_min = command_latency; 
			if (command_latency > pal_read_lat_max) 
				pal_read_lat_max = command_latency; 

			pal_read_lat_avg = pal_read_lat_avg * ((double)pal_read_req_count / (pal_read_req_count + 1)) + (command_latency / (double)(pal_read_req_count + 1)); 	
			pal_read_req_count++; 
	    	pal_read_capacity += ftl->param->page_size; 
			break;
		case OP_WRITE: 
			if (command_latency < pal_write_lat_min)
				pal_write_lat_min = command_latency; 
			if (command_latency > pal_write_lat_max) 
				pal_write_lat_max = command_latency; 

			pal_write_lat_avg = pal_write_lat_avg * ((double)pal_write_req_count / (pal_write_req_count + 1)) + (command_latency / (double)(pal_write_req_count + 1)); 	
			pal_write_req_count++; 
	    	pal_write_capacity += ftl->param->page_size; 
	
			break; 
		case OP_ERASE: 
			if (command_latency < pal_erase_lat_min)
				pal_erase_lat_min = command_latency; 
			if (command_latency > pal_erase_lat_max) 
				pal_erase_lat_max = command_latency; 

			pal_erase_lat_avg = pal_erase_lat_avg * ((double)pal_erase_req_count / (pal_erase_req_count + 1)) + (command_latency / (double)(pal_erase_req_count + 1)); 	

			pal_erase_req_count++; 
			break; 
	}
}
void CommandQueue::set_queue_file(const char * output_file_name){
	que_file.open(output_file_name); 
}
