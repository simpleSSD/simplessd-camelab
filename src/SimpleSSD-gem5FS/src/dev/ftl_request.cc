#include "dev/ftl_request.hh"


InputRequest::InputRequest(Tick arrive_time, int op, Addr address, Addr size, int ps){
	time = arrive_time; 
	operation = op; 
	sector_address = address; 
	sector_size = size; 

	Addr start_lpn = sector_address / ps; 
	Addr end_lpn = (sector_address + sector_size - 1) / ps; 
	
	page_address = start_lpn; 
	page_size = end_lpn - start_lpn + 1;

	service_latency = 0; 

	serviced_page = 0; 
	updated_page = 0; 
	done = false; 
}
bool InputRequest::page_service_done(Tick latency){ // return true if all page requests has been completed 
	if (latency > service_latency)
		service_latency = latency; 
	serviced_page++; 
	if (serviced_page == page_size){
		done = true; 
		return true; 
	}
	return false;
}

bool InputRequest::page_status_updated(){
	updated_page++; 
	if (updated_page == page_size){
		return true; 
	}
	return false; 
}

OutputRequest::OutputRequest(long int sequence_number, Tick arrive_time, Addr lpn, Addr ppn, InputRequest * parent, int op){
	seq_num = sequence_number; 
	time = arrive_time; 
	operation = op; 
	logical_page_address = lpn; 
	parent_req = parent; 
	physical_page_address = ppn; 
	latency_reported = false; 
}
