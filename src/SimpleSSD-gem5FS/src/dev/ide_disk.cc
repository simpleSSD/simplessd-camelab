/*
 * Copyright (c) 2013 ARM Limited
 * All rights reserved
 *
 * The license below extends only to copyright in the software and shall
 * not be construed as granting a license to any other intellectual
 * property including but not limited to intellectual property relating
 * to a hardware implementation of the functionality of the software
 * licensed hereunder.  You may use the software subject to the license
 * terms below provided that you ensure that this notice is replicated
 * unmodified and in its entirety in all distributions of the software,
 * modified or unmodified, in source code or in binary form.
 *
 * Copyright (c) 2004-2005 The Regents of The University of Michigan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Andrew Schultz
 *          Ali Saidi
 */

/** @file
 * Device model implementation for an IDE disk
 */

#include <cerrno>
#include <cstring>
#include <deque>
#include <string>

#include "arch/isa_traits.hh"
#include "base/chunk_generator.hh"
#include "base/cprintf.hh" // csprintf
#include "base/trace.hh"
#include "config/the_isa.hh"
#include "debug/IdeDisk.hh"
#include "dev/disk_image.hh"
#include "dev/ide_ctrl.hh"
#include "dev/ide_disk.hh"
#include "dev/hil.hh"     ////////////////////////////////////////Ahmed Abulila
#include "sim/core.hh"
#include "sim/sim_object.hh"

using namespace std;
using namespace TheISA;

IdeDisk::IdeDisk(const Params *p)
  : SimObject(p), ctrl(NULL), image(p->image), diskDelay(p->delay),
      dmaTransferEvent(this), dmaReadCG(NULL), dmaReadWaitEvent(this),
      dmaWriteCG(NULL), dmaWriteWaitEvent(this), dmaPrdReadEvent(this),
    dmaReadEvent(this), dmaWriteEvent(this), schPrintEvent(this) //((p->driveID=='master')?hil(0):hil(1))//, SSDenable(p->SSDenable)
{
    SSDenable = p->SSDenable;
    // Reset the device state
    reset(p->driveID);
   
    // fill out the drive ID structure
    memset(&driveID, 0, sizeof(struct ataparams));

    // Calculate LBA and C/H/S values
    uint16_t cylinders;
    uint8_t heads;
    uint8_t sectors;
   
    uint32_t lba_size = image->size();
    if (lba_size >= 16383*16*63) {
        cylinders = 16383;
        heads = 16;
        sectors = 63;
    } else {
        if (lba_size >= 63)
            sectors = 63;
        else
            sectors = lba_size;

        if ((lba_size / sectors) >= 16)
            heads = 16;
        else
            heads = (lba_size / sectors);

        cylinders = lba_size / (heads * sectors);
    }


    //hil(); ///////////////////////////////////////Ahmed Abulila
    
    if (strcmp(name().c_str() , "system.cf0") == 0) { 	
      hil = new HIL(0, SSDenable, p->SSDConfig);//hil.setDiskNo(0);//hil = new HIL(0);
      //schedPrint();
    } else {  	
      hil = new HIL(1, SSDenable, p->SSDConfig);//hil.setDiskNo(0);//hil = new HIL(0);
    }
    schedule(schPrintEvent, 50000000000);
    


	// Setup the model name
    strncpy((char *)driveID.atap_model, "5MI EDD si k",
            sizeof(driveID.atap_model));
    // Set the maximum multisector transfer size
    driveID.atap_multi = MAX_MULTSECT;
    // IORDY supported, IORDY disabled, LBA enabled, DMA enabled
    driveID.atap_capabilities1 = 0x7;
    // UDMA support, EIDE support
    driveID.atap_extensions = 0x6;
    // Setup default C/H/S settings
    driveID.atap_cylinders = cylinders;
    driveID.atap_sectors = sectors;
    driveID.atap_heads = heads;
    // Setup the current multisector transfer size
    driveID.atap_curmulti = MAX_MULTSECT;
    driveID.atap_curmulti_valid = 0x1;
    // Number of sectors on disk
    driveID.atap_capacity = lba_size;
    // Multiword DMA mode 2 and below supported
    driveID.atap_dmamode_supp = 0x4;
    // Set PIO mode 4 and 3 supported
    driveID.atap_piomode_supp = 0x3;
    // Set DMA mode 4 and below supported
    driveID.atap_udmamode_supp = 0x1f;
    // Statically set hardware config word
    driveID.atap_hwreset_res = 0x4001;

    //arbitrary for now...
    driveID.atap_ata_major = WDC_VER_ATA7;


}

IdeDisk::~IdeDisk()
{
    // destroy the data buffer
    delete [] dataBuffer;
}

void
IdeDisk::statsUpdate()
{

  if(SSDenable) {
    hil->get_parameter(PAL_LAYER, SSD_Stats);
    updatePALStats();
    hil->get_parameter(FTL_HOST_LAYER, SSD_Stats);
    updateFTLStats();
    hil->get_parameter(FTL_PAL_LAYER, SSD_Stats);
    updateFTL_PAL_Stats();
    hil->get_parameter(FTL_MAP_LAYER, SSD_Stats);
    updateFTL_MAP_Stats();
  } else {
    hil->get_parameter(HDD_LAYER, SSD_Stats);
    updateHDDStats();
  }
}



void
IdeDisk::updateHDDStats()
{
    HDD_CAP_RD_AVG = SSD_Stats.statistics[RD][CAPACITY][AVG];
    HDD_CAP_RD_MIN = SSD_Stats.statistics[RD][CAPACITY][MIN];
    HDD_CAP_RD_MAX = SSD_Stats.statistics[RD][CAPACITY][MAX];
    HDD_CAP_RD_TOT = SSD_Stats.statistics[RD][CAPACITY][TOT];
    HDD_CAP_RD_COUNT = SSD_Stats.statistics[RD][CAPACITY][COUNT];
    HDD_CAP_WR_AVG = SSD_Stats.statistics[WR][CAPACITY][AVG];
    HDD_CAP_WR_MIN = SSD_Stats.statistics[WR][CAPACITY][MIN];
    HDD_CAP_WR_MAX = SSD_Stats.statistics[WR][CAPACITY][MAX];
    HDD_CAP_WR_TOT = SSD_Stats.statistics[WR][CAPACITY][TOT];
    HDD_CAP_WR_COUNT = SSD_Stats.statistics[WR][CAPACITY][COUNT];
    HDD_CAP_TOT_AVG = SSD_Stats.statistics[ALL][CAPACITY][AVG];
    HDD_CAP_TOT_MIN = SSD_Stats.statistics[ALL][CAPACITY][MIN];
    HDD_CAP_TOT_MAX = SSD_Stats.statistics[ALL][CAPACITY][MAX];
    HDD_CAP_TOT_TOT = SSD_Stats.statistics[ALL][CAPACITY][TOT];
    HDD_CAP_TOT_COUNT = SSD_Stats.statistics[ALL][CAPACITY][COUNT];
    HDD_BW_RD_AVG = SSD_Stats.statistics[RD][BANDWIDTH][AVG];
    HDD_BW_RD_MIN = SSD_Stats.statistics[RD][BANDWIDTH][MIN];
    HDD_BW_RD_MAX = SSD_Stats.statistics[RD][BANDWIDTH][MAX];
    HDD_BW_RD_AVG_WIDLE = SSD_Stats.statistics[RD][BANDWIDTH_WIDLE][AVG];
    HDD_BW_RD_MIN_WIDLE = SSD_Stats.statistics[RD][BANDWIDTH_WIDLE][MIN];
    HDD_BW_RD_MAX_WIDLE = SSD_Stats.statistics[RD][BANDWIDTH_WIDLE][MAX];
    HDD_BW_RD_AVG_ONLY = SSD_Stats.statistics[RD][BANDWIDTH_OPER][AVG];
    HDD_BW_RD_MIN_ONLY = SSD_Stats.statistics[RD][BANDWIDTH_OPER][MIN];
    HDD_BW_RD_MAX_ONLY = SSD_Stats.statistics[RD][BANDWIDTH_OPER][MAX];
    HDD_BW_WR_AVG = SSD_Stats.statistics[WR][BANDWIDTH][AVG];
    HDD_BW_WR_MIN = SSD_Stats.statistics[WR][BANDWIDTH][MIN];
    HDD_BW_WR_MAX = SSD_Stats.statistics[WR][BANDWIDTH][MAX];
    HDD_BW_WR_AVG_WIDLE = SSD_Stats.statistics[WR][BANDWIDTH_WIDLE][AVG];
    HDD_BW_WR_MIN_WIDLE = SSD_Stats.statistics[WR][BANDWIDTH_WIDLE][MIN];
    HDD_BW_WR_MAX_WIDLE = SSD_Stats.statistics[WR][BANDWIDTH_WIDLE][MAX];
    HDD_BW_WR_AVG_ONLY = SSD_Stats.statistics[WR][BANDWIDTH_OPER][AVG];
    HDD_BW_WR_MIN_ONLY = SSD_Stats.statistics[WR][BANDWIDTH_OPER][MIN];
    HDD_BW_WR_MAX_ONLY = SSD_Stats.statistics[WR][BANDWIDTH_OPER][MAX];
    HDD_LAT_RD_AVG = SSD_Stats.statistics[RD][LATENCY][AVG];
    HDD_LAT_RD_MIN = SSD_Stats.statistics[RD][LATENCY][MIN];
    HDD_LAT_RD_MAX = SSD_Stats.statistics[RD][LATENCY][MAX];
    HDD_LAT_WR_AVG = SSD_Stats.statistics[WR][LATENCY][AVG];
    HDD_LAT_WR_MIN = SSD_Stats.statistics[WR][LATENCY][MIN];
    HDD_LAT_WR_MAX = SSD_Stats.statistics[WR][LATENCY][MAX];
    HDD_IO_RD_AVG = SSD_Stats.statistics[RD][IOPS][AVG];
    HDD_IO_RD_MIN = SSD_Stats.statistics[RD][IOPS][MIN];
    HDD_IO_RD_MAX = SSD_Stats.statistics[RD][IOPS][MAX];
    HDD_IO_RD_AVG_WIDLE = SSD_Stats.statistics[RD][IOPS_WIDLE][AVG];
    HDD_IO_RD_MIN_WIDLE = SSD_Stats.statistics[RD][IOPS_WIDLE][MIN];
    HDD_IO_RD_MAX_WIDLE = SSD_Stats.statistics[RD][IOPS_WIDLE][MAX];
    HDD_IO_RD_AVG_ONLY = SSD_Stats.statistics[RD][IOPS_OPER][AVG];
    HDD_IO_RD_MIN_ONLY = SSD_Stats.statistics[RD][IOPS_OPER][MIN];
    HDD_IO_RD_MAX_ONLY = SSD_Stats.statistics[RD][IOPS_OPER][MAX];
    HDD_IO_WR_AVG = SSD_Stats.statistics[WR][IOPS][AVG];
    HDD_IO_WR_MIN = SSD_Stats.statistics[WR][IOPS][MIN];
    HDD_IO_WR_MAX = SSD_Stats.statistics[WR][IOPS][MAX];
    HDD_IO_WR_AVG_WIDLE = SSD_Stats.statistics[WR][IOPS_WIDLE][AVG];
    HDD_IO_WR_MIN_WIDLE = SSD_Stats.statistics[WR][IOPS_WIDLE][MIN];
    HDD_IO_WR_MAX_WIDLE = SSD_Stats.statistics[WR][IOPS_WIDLE][MAX];
    HDD_IO_WR_AVG_ONLY = SSD_Stats.statistics[WR][IOPS_OPER][AVG];
    HDD_IO_WR_MIN_ONLY = SSD_Stats.statistics[WR][IOPS_OPER][MIN];
    HDD_IO_WR_MAX_ONLY = SSD_Stats.statistics[WR][IOPS_OPER][MAX];
    HDD_DEVICE_IDLE = SSD_Stats.statistics[TOT][DEVICE_IDLE][TOT];
    HDD_DEVICE_BUSY = SSD_Stats.statistics[TOT][DEVICE_BUSY][TOT];

}


void
IdeDisk::updatePALStats()
{
    PAL_CAP_RD_AVG   = SSD_Stats.statistics[RD][CAPACITY][AVG];
    PAL_CAP_RD_MIN   = SSD_Stats.statistics[RD][CAPACITY][MIN];
    PAL_CAP_RD_MAX   = SSD_Stats.statistics[RD][CAPACITY][MAX];
    PAL_CAP_RD_TOT   = SSD_Stats.statistics[RD][CAPACITY][TOT];
    PAL_CAP_RD_COUNT = SSD_Stats.statistics[RD][CAPACITY][COUNT];
    PAL_BW_RD_AVG    = SSD_Stats.statistics[RD][BANDWIDTH][AVG];
    PAL_BW_RD_MIN    = SSD_Stats.statistics[RD][BANDWIDTH][MIN];
    PAL_BW_RD_MAX    = SSD_Stats.statistics[RD][BANDWIDTH][MAX];
    PAL_BW_RD_AVG_WIDLE    = SSD_Stats.statistics[RD][BANDWIDTH_WIDLE][AVG];
    PAL_BW_RD_MIN_WIDLE    = SSD_Stats.statistics[RD][BANDWIDTH_WIDLE][MIN];
    PAL_BW_RD_MAX_WIDLE    = SSD_Stats.statistics[RD][BANDWIDTH_WIDLE][MAX];
    PAL_BW_RD_AVG_ONLY    = SSD_Stats.statistics[RD][BANDWIDTH_OPER][AVG];
    PAL_BW_RD_MIN_ONLY    = SSD_Stats.statistics[RD][BANDWIDTH_OPER][MIN];
    PAL_BW_RD_MAX_ONLY    = SSD_Stats.statistics[RD][BANDWIDTH_OPER][MAX];
    PAL_IO_RD_AVG    = SSD_Stats.statistics[RD][IOPS][AVG];
    PAL_IO_RD_MIN    = SSD_Stats.statistics[RD][IOPS][MIN];
    PAL_IO_RD_MAX    = SSD_Stats.statistics[RD][IOPS][MAX];
    PAL_IO_RD_AVG_WIDLE    = SSD_Stats.statistics[RD][IOPS_WIDLE][AVG];
    PAL_IO_RD_MIN_WIDLE    = SSD_Stats.statistics[RD][IOPS_WIDLE][MIN];
    PAL_IO_RD_MAX_WIDLE    = SSD_Stats.statistics[RD][IOPS_WIDLE][MAX];
    PAL_IO_RD_AVG_ONLY    = SSD_Stats.statistics[RD][IOPS_OPER][AVG];
    PAL_IO_RD_MIN_ONLY    = SSD_Stats.statistics[RD][IOPS_OPER][MIN];
    PAL_IO_RD_MAX_ONLY    = SSD_Stats.statistics[RD][IOPS_OPER][MAX];
    PAL_LAT_RD_AVG    = SSD_Stats.statistics[RD][LATENCY][AVG]/1000/1000;
    PAL_LAT_RD_MIN    = SSD_Stats.statistics[RD][LATENCY][MIN]/1000/1000;
    PAL_LAT_RD_MAX    = SSD_Stats.statistics[RD][LATENCY][MAX]/1000/1000;

    PAL_CAP_WR_AVG   = SSD_Stats.statistics[WR][CAPACITY][AVG];
    PAL_CAP_WR_MIN   = SSD_Stats.statistics[WR][CAPACITY][MIN];
    PAL_CAP_WR_MAX   = SSD_Stats.statistics[WR][CAPACITY][MAX];
    PAL_CAP_WR_TOT   = SSD_Stats.statistics[WR][CAPACITY][TOT];
    PAL_CAP_WR_COUNT = SSD_Stats.statistics[WR][CAPACITY][COUNT];
    PAL_BW_WR_AVG    = SSD_Stats.statistics[WR][BANDWIDTH][AVG];
    PAL_BW_WR_MIN    = SSD_Stats.statistics[WR][BANDWIDTH][MIN];
    PAL_BW_WR_MAX    = SSD_Stats.statistics[WR][BANDWIDTH][MAX];
    PAL_BW_WR_AVG_WIDLE    = SSD_Stats.statistics[WR][BANDWIDTH_WIDLE][AVG];
    PAL_BW_WR_MIN_WIDLE    = SSD_Stats.statistics[WR][BANDWIDTH_WIDLE][MIN];
    PAL_BW_WR_MAX_WIDLE    = SSD_Stats.statistics[WR][BANDWIDTH_WIDLE][MAX];
    PAL_BW_WR_AVG_ONLY    = SSD_Stats.statistics[WR][BANDWIDTH_OPER][AVG];
    PAL_BW_WR_MIN_ONLY    = SSD_Stats.statistics[WR][BANDWIDTH_OPER][MIN];
    PAL_BW_WR_MAX_ONLY    = SSD_Stats.statistics[WR][BANDWIDTH_OPER][MAX];
    PAL_IO_WR_AVG    = SSD_Stats.statistics[WR][IOPS][AVG];
    PAL_IO_WR_MIN    = SSD_Stats.statistics[WR][IOPS][MIN];
    PAL_IO_WR_MAX    = SSD_Stats.statistics[WR][IOPS][MAX];
    PAL_IO_WR_AVG_WIDLE    = SSD_Stats.statistics[WR][IOPS_WIDLE][AVG];
    PAL_IO_WR_MIN_WIDLE    = SSD_Stats.statistics[WR][IOPS_WIDLE][MIN];
    PAL_IO_WR_MAX_WIDLE    = SSD_Stats.statistics[WR][IOPS_WIDLE][MAX];
    PAL_IO_WR_AVG_ONLY    = SSD_Stats.statistics[WR][IOPS_OPER][AVG];
    PAL_IO_WR_MIN_ONLY    = SSD_Stats.statistics[WR][IOPS_OPER][MIN];
    PAL_IO_WR_MAX_ONLY    = SSD_Stats.statistics[WR][IOPS_OPER][MAX];
    PAL_LAT_WR_AVG    = SSD_Stats.statistics[WR][LATENCY][AVG]/1000/1000;
    PAL_LAT_WR_MIN    = SSD_Stats.statistics[WR][LATENCY][MIN]/1000/1000;
    PAL_LAT_WR_MAX    = SSD_Stats.statistics[WR][LATENCY][MAX]/1000/1000;

    PAL_CAP_ER_AVG   = SSD_Stats.statistics[ER][CAPACITY][AVG];
    PAL_CAP_ER_MIN   = SSD_Stats.statistics[ER][CAPACITY][MIN];
    PAL_CAP_ER_MAX   = SSD_Stats.statistics[ER][CAPACITY][MAX];
    PAL_CAP_ER_TOT   = SSD_Stats.statistics[ER][CAPACITY][TOT];
    PAL_CAP_ER_COUNT = SSD_Stats.statistics[ER][CAPACITY][COUNT];
    PAL_BW_ER_AVG    = SSD_Stats.statistics[ER][BANDWIDTH][AVG];
    PAL_BW_ER_MIN    = SSD_Stats.statistics[ER][BANDWIDTH][MIN];
    PAL_BW_ER_MAX    = SSD_Stats.statistics[ER][BANDWIDTH][MAX];
    PAL_BW_ER_AVG_WIDLE    = SSD_Stats.statistics[ER][BANDWIDTH_WIDLE][AVG];
    PAL_BW_ER_MIN_WIDLE    = SSD_Stats.statistics[ER][BANDWIDTH_WIDLE][MIN];
    PAL_BW_ER_MAX_WIDLE    = SSD_Stats.statistics[ER][BANDWIDTH_WIDLE][MAX];
    PAL_BW_ER_AVG_ONLY    = SSD_Stats.statistics[ER][BANDWIDTH_OPER][AVG];
    PAL_BW_ER_MIN_ONLY    = SSD_Stats.statistics[ER][BANDWIDTH_OPER][MIN];
    PAL_BW_ER_MAX_ONLY    = SSD_Stats.statistics[ER][BANDWIDTH_OPER][MAX];
    PAL_IO_ER_AVG    = SSD_Stats.statistics[ER][IOPS][AVG];
    PAL_IO_ER_MIN    = SSD_Stats.statistics[ER][IOPS][MIN];
    PAL_IO_ER_MAX    = SSD_Stats.statistics[ER][IOPS][MAX];
    PAL_IO_ER_AVG_WIDLE    = SSD_Stats.statistics[ER][IOPS_WIDLE][AVG];
    PAL_IO_ER_MIN_WIDLE    = SSD_Stats.statistics[ER][IOPS_WIDLE][MIN];
    PAL_IO_ER_MAX_WIDLE    = SSD_Stats.statistics[ER][IOPS_WIDLE][MAX];
    PAL_IO_ER_AVG_ONLY    = SSD_Stats.statistics[ER][IOPS_OPER][AVG];
    PAL_IO_ER_MIN_ONLY    = SSD_Stats.statistics[ER][IOPS_OPER][MIN];
    PAL_IO_ER_MAX_ONLY    = SSD_Stats.statistics[ER][IOPS_OPER][MAX];
    PAL_LAT_ER_AVG    = SSD_Stats.statistics[ER][LATENCY][AVG]/1000/1000;
    PAL_LAT_ER_MIN    = SSD_Stats.statistics[ER][LATENCY][MIN]/1000/1000;
    PAL_LAT_ER_MAX    = SSD_Stats.statistics[ER][LATENCY][MAX]/1000/1000;

    PAL_CAP_TOT_AVG   = SSD_Stats.statistics[TOT][CAPACITY][AVG];
    PAL_CAP_TOT_MIN   = SSD_Stats.statistics[TOT][CAPACITY][MIN];
    PAL_CAP_TOT_MAX   = SSD_Stats.statistics[TOT][CAPACITY][MAX];
    PAL_CAP_TOT_TOT   = SSD_Stats.statistics[TOT][CAPACITY][TOT];
    PAL_CAP_TOT_COUNT = SSD_Stats.statistics[TOT][CAPACITY][COUNT];
    PAL_BW_TOT_AVG    = SSD_Stats.statistics[TOT][BANDWIDTH][AVG];
    PAL_BW_TOT_MIN    = SSD_Stats.statistics[TOT][BANDWIDTH][MIN];
    PAL_BW_TOT_MAX    = SSD_Stats.statistics[TOT][BANDWIDTH][MAX];
    PAL_BW_TOT_AVG_WIDLE    = SSD_Stats.statistics[TOT][BANDWIDTH_WIDLE][AVG];
    PAL_BW_TOT_MIN_WIDLE    = SSD_Stats.statistics[TOT][BANDWIDTH_WIDLE][MIN];
    PAL_BW_TOT_MAX_WIDLE    = SSD_Stats.statistics[TOT][BANDWIDTH_WIDLE][MAX];
    PAL_IO_TOT_AVG    = SSD_Stats.statistics[TOT][IOPS][AVG];
    PAL_IO_TOT_MIN    = SSD_Stats.statistics[TOT][IOPS][MIN];
    PAL_IO_TOT_MAX    = SSD_Stats.statistics[TOT][IOPS][MAX];
    PAL_IO_TOT_AVG_WIDLE    = SSD_Stats.statistics[TOT][IOPS_WIDLE][AVG];
    PAL_IO_TOT_MIN_WIDLE    = SSD_Stats.statistics[TOT][IOPS_WIDLE][MIN];
    PAL_IO_TOT_MAX_WIDLE    = SSD_Stats.statistics[TOT][IOPS_WIDLE][MAX];
    PAL_LAT_TOT_AVG    = SSD_Stats.statistics[TOT][LATENCY][AVG]/1000/1000;
    PAL_LAT_TOT_MIN    = SSD_Stats.statistics[TOT][LATENCY][MIN]/1000/1000;
    PAL_LAT_TOT_MAX    = SSD_Stats.statistics[TOT][LATENCY][MAX]/1000/1000;

    PAL_DEVICE_IDLE = SSD_Stats.statistics[TOT][DEVICE_IDLE][TOT];
    PAL_DEVICE_BUSY = SSD_Stats.statistics[TOT][DEVICE_BUSY][TOT];
}


void
IdeDisk::updateFTLStats()
{
    FTL_HOST_CAP_RD_TOT   = SSD_Stats.statistics[RD][CAPACITY][TOT];
    FTL_HOST_CAP_RD_COUNT = SSD_Stats.statistics[RD][CAPACITY][COUNT];
    FTL_HOST_BW_RD_AVG    = SSD_Stats.statistics[RD][BANDWIDTH][AVG];
    FTL_HOST_BW_RD_MIN    = SSD_Stats.statistics[RD][BANDWIDTH][MIN];
    FTL_HOST_BW_RD_MAX    = SSD_Stats.statistics[RD][BANDWIDTH][MAX];
    FTL_HOST_LAT_RD_AVG    = SSD_Stats.statistics[RD][LATENCY][AVG];
    FTL_HOST_LAT_RD_MIN    = SSD_Stats.statistics[RD][LATENCY][MIN];
    FTL_HOST_LAT_RD_MAX    = SSD_Stats.statistics[RD][LATENCY][MAX];
    FTL_HOST_IO_RD_AVG    = SSD_Stats.statistics[RD][IOPS][AVG];
    FTL_HOST_IO_RD_MIN    = SSD_Stats.statistics[RD][IOPS][MIN];
    FTL_HOST_IO_RD_MAX    = SSD_Stats.statistics[RD][IOPS][MAX];
    FTL_HOST_SIZE_RD_AVG    = SSD_Stats.statistics[RD][SIZE][AVG];
    FTL_HOST_SIZE_RD_MIN    = SSD_Stats.statistics[RD][SIZE][MIN];
    FTL_HOST_SIZE_RD_MAX    = SSD_Stats.statistics[RD][SIZE][MAX];

    FTL_HOST_CAP_WR_TOT   = SSD_Stats.statistics[WR][CAPACITY][TOT];
    FTL_HOST_CAP_WR_COUNT = SSD_Stats.statistics[WR][CAPACITY][COUNT];
    FTL_HOST_BW_WR_AVG    = SSD_Stats.statistics[WR][BANDWIDTH][AVG];
    FTL_HOST_BW_WR_MIN    = SSD_Stats.statistics[WR][BANDWIDTH][MIN];
    FTL_HOST_BW_WR_MAX    = SSD_Stats.statistics[WR][BANDWIDTH][MAX];
    FTL_HOST_LAT_WR_AVG    = SSD_Stats.statistics[WR][LATENCY][AVG];
    FTL_HOST_LAT_WR_MIN    = SSD_Stats.statistics[WR][LATENCY][MIN];
    FTL_HOST_LAT_WR_MAX    = SSD_Stats.statistics[WR][LATENCY][MAX];
    FTL_HOST_IO_WR_AVG    = SSD_Stats.statistics[WR][IOPS][AVG];
    FTL_HOST_IO_WR_MIN    = SSD_Stats.statistics[WR][IOPS][MIN];
    FTL_HOST_IO_WR_MAX    = SSD_Stats.statistics[WR][IOPS][MAX];
    FTL_HOST_SIZE_WR_AVG    = SSD_Stats.statistics[WR][SIZE][AVG];
    FTL_HOST_SIZE_WR_MIN    = SSD_Stats.statistics[WR][SIZE][MIN];
    FTL_HOST_SIZE_WR_MAX    = SSD_Stats.statistics[WR][SIZE][MAX];
    
    FTL_HOST_BW_TOT_AVG_WIDLE 	= SSD_Stats.statistics[TOT][BANDWIDTH_WIDLE][AVG]; 
    FTL_HOST_BW_TOT_MIN_WIDLE 	= SSD_Stats.statistics[TOT][BANDWIDTH_WIDLE][MIN]; 
    FTL_HOST_BW_TOT_MAX_WIDLE 	= SSD_Stats.statistics[TOT][BANDWIDTH_WIDLE][MAX]; 
    FTL_HOST_BW_TOT_AVG		 	= SSD_Stats.statistics[TOT][BANDWIDTH][AVG]; 
    FTL_HOST_BW_TOT_MIN		 	= SSD_Stats.statistics[TOT][BANDWIDTH][MIN]; 
    FTL_HOST_BW_TOT_MAX		 	= SSD_Stats.statistics[TOT][BANDWIDTH][MAX]; 
    
    FTL_HOST_IO_TOT_AVG_WIDLE 	= SSD_Stats.statistics[TOT][IOPS_WIDLE][AVG]; 
    FTL_HOST_IO_TOT_MIN_WIDLE 	= SSD_Stats.statistics[TOT][IOPS_WIDLE][MIN]; 
    FTL_HOST_IO_TOT_MAX_WIDLE 	= SSD_Stats.statistics[TOT][IOPS_WIDLE][MAX]; 
    FTL_HOST_IO_TOT_AVG		 	= SSD_Stats.statistics[TOT][IOPS][AVG]; 
    FTL_HOST_IO_TOT_MIN		 	= SSD_Stats.statistics[TOT][IOPS][MIN]; 
    FTL_HOST_IO_TOT_MAX		 	= SSD_Stats.statistics[TOT][IOPS][MAX]; 
	
}

void
IdeDisk::updateFTL_PAL_Stats()
{
    FTL_PAL_CAP_RD_TOT   = SSD_Stats.statistics[RD][CAPACITY][TOT];
    FTL_PAL_CAP_RD_COUNT = SSD_Stats.statistics[RD][CAPACITY][COUNT];
    FTL_PAL_CAP_WR_TOT   = SSD_Stats.statistics[WR][CAPACITY][TOT];
    FTL_PAL_CAP_WR_COUNT = SSD_Stats.statistics[WR][CAPACITY][COUNT];
    FTL_PAL_CAP_ER_COUNT = SSD_Stats.statistics[ER][CAPACITY][COUNT];
    FTL_PAL_LAT_RD_AVG    = SSD_Stats.statistics[RD][LATENCY][AVG];
    FTL_PAL_LAT_RD_MIN    = SSD_Stats.statistics[RD][LATENCY][MIN];
    FTL_PAL_LAT_RD_MAX    = SSD_Stats.statistics[RD][LATENCY][MAX];
    FTL_PAL_LAT_WR_AVG    = SSD_Stats.statistics[WR][LATENCY][AVG];
    FTL_PAL_LAT_WR_MIN    = SSD_Stats.statistics[WR][LATENCY][MIN];
    FTL_PAL_LAT_WR_MAX    = SSD_Stats.statistics[WR][LATENCY][MAX];
    FTL_PAL_LAT_ER_AVG    = SSD_Stats.statistics[ER][LATENCY][AVG];
    FTL_PAL_LAT_ER_MIN    = SSD_Stats.statistics[ER][LATENCY][MIN];
    FTL_PAL_LAT_ER_MAX    = SSD_Stats.statistics[ER][LATENCY][MAX];
}

void
IdeDisk::updateFTL_MAP_Stats()
{
    FTL_MAP_CAP_GC_COUNT = SSD_Stats.statistics[GC][CAPACITY][COUNT];
    FTL_MAP_CAP_ER_COUNT = SSD_Stats.statistics[ER][CAPACITY][COUNT];
    FTL_MAP_LAT_GC_AVG    = SSD_Stats.statistics[GC][LATENCY][AVG];
    FTL_MAP_LAT_GC_MIN    = SSD_Stats.statistics[GC][LATENCY][MIN];
    FTL_MAP_LAT_GC_MAX    = SSD_Stats.statistics[GC][LATENCY][MAX];
}


void
IdeDisk::schedPrint()
{
  hil->printStats();
  schedule(schPrintEvent, curTick() + 100000000000); 
}

void
IdeDisk::reset(int id)
{
    // initialize the data buffer and shadow registers
    dataBuffer = new uint8_t[MAX_DMA_SIZE];

    memset(dataBuffer, 0, MAX_DMA_SIZE);
    memset(&cmdReg, 0, sizeof(CommandReg_t));
    memset(&curPrd.entry, 0, sizeof(PrdEntry_t));

    curPrdAddr = 0;
    curSector = 0;
    cmdBytes = 0;
    cmdBytesLeft = 0;
    drqBytesLeft = 0;
    dmaRead = false;
    intrPending = false;
    dmaAborted = false;

    // set the device state to idle
    dmaState = Dma_Idle;

    if (id == DEV0) {
        devState = Device_Idle_S;
        devID = DEV0;
    } else if (id == DEV1) {
        devState = Device_Idle_NS;
        devID = DEV1;
    } else {
        panic("Invalid device ID: %#x\n", id);
    }

    //printf("***********SSDenable is %d ****************\n", SSDenable);

    // set the device ready bit
    status = STATUS_DRDY_BIT;

    /* The error register must be set to 0x1 on start-up to
       indicate that no diagnostic error was detected */
    cmdReg.error = 0x1;
}

////
// Utility functions
////

bool
IdeDisk::isDEVSelect()
{
    return ctrl->isDiskSelected(this);
}

Addr
IdeDisk::pciToDma(Addr pciAddr)
{
    if (ctrl)
        return ctrl->pciToDma(pciAddr);
    else
        panic("Access to unset controller!\n");
}

////
// Device registers read/write
////

void
IdeDisk::readCommand(const Addr offset, int size, uint8_t *data)
{
    if (offset == DATA_OFFSET) {
        if (size == sizeof(uint16_t)) {
            *(uint16_t *)data = cmdReg.data;
        } else if (size == sizeof(uint32_t)) {
            *(uint16_t *)data = cmdReg.data;
            updateState(ACT_DATA_READ_SHORT);
            *((uint16_t *)data + 1) = cmdReg.data;
        } else {
            panic("Data read of unsupported size %d.\n", size);
        }
        updateState(ACT_DATA_READ_SHORT);
        return;
    }
    assert(size == sizeof(uint8_t));
    switch (offset) {
      case ERROR_OFFSET:
        *data = cmdReg.error;
        break;
      case NSECTOR_OFFSET:
        *data = cmdReg.sec_count;
        break;
      case SECTOR_OFFSET:
        *data = cmdReg.sec_num;
        break;
      case LCYL_OFFSET:
        *data = cmdReg.cyl_low;
        break;
      case HCYL_OFFSET:
        *data = cmdReg.cyl_high;
        break;
      case DRIVE_OFFSET:
        *data = cmdReg.drive;
        break;
      case STATUS_OFFSET:
        *data = status;
        updateState(ACT_STAT_READ);
        break;
      default:
        panic("Invalid IDE command register offset: %#x\n", offset);
    }
    DPRINTF(IdeDisk, "Read to disk at offset: %#x data %#x\n", offset, *data);
}

void
IdeDisk::readControl(const Addr offset, int size, uint8_t *data)
{
    assert(size == sizeof(uint8_t));
    *data = status;
    if (offset != ALTSTAT_OFFSET)
        panic("Invalid IDE control register offset: %#x\n", offset);
    DPRINTF(IdeDisk, "Read to disk at offset: %#x data %#x\n", offset, *data);
}

void
IdeDisk::writeCommand(const Addr offset, int size, const uint8_t *data)
{
    if (offset == DATA_OFFSET) {
        if (size == sizeof(uint16_t)) {
            cmdReg.data = *(const uint16_t *)data;
        } else if (size == sizeof(uint32_t)) {
            cmdReg.data = *(const uint16_t *)data;
            updateState(ACT_DATA_WRITE_SHORT);
            cmdReg.data = *((const uint16_t *)data + 1);
        } else {
            panic("Data write of unsupported size %d.\n", size);
        }
        updateState(ACT_DATA_WRITE_SHORT);
        return;
    }

    assert(size == sizeof(uint8_t));
    switch (offset) {
      case FEATURES_OFFSET:
        break;
      case NSECTOR_OFFSET:
        cmdReg.sec_count = *data;
        break;
      case SECTOR_OFFSET:
        cmdReg.sec_num = *data;
        break;
      case LCYL_OFFSET:
        cmdReg.cyl_low = *data;
        break;
      case HCYL_OFFSET:
        cmdReg.cyl_high = *data;
        break;
      case DRIVE_OFFSET:
        cmdReg.drive = *data;
        updateState(ACT_SELECT_WRITE);
        break;
      case COMMAND_OFFSET:
        cmdReg.command = *data;
        updateState(ACT_CMD_WRITE);
        break;
      default:
        panic("Invalid IDE command register offset: %#x\n", offset);
    }
    DPRINTF(IdeDisk, "Write to disk at offset: %#x data %#x\n", offset,
            (uint32_t)*data);
}

void
IdeDisk::writeControl(const Addr offset, int size, const uint8_t *data)
{
    if (offset != CONTROL_OFFSET)
        panic("Invalid IDE control register offset: %#x\n", offset);

    if (*data & CONTROL_RST_BIT) {
        // force the device into the reset state
        devState = Device_Srst;
        updateState(ACT_SRST_SET);
    } else if (devState == Device_Srst && !(*data & CONTROL_RST_BIT)) {
        updateState(ACT_SRST_CLEAR);
    }

    nIENBit = *data & CONTROL_IEN_BIT;

    DPRINTF(IdeDisk, "Write to disk at offset: %#x data %#x\n", offset,
            (uint32_t)*data);
}

////
// Perform DMA transactions
////

void
IdeDisk::doDmaTransfer()
{
    if (dmaAborted) {
        DPRINTF(IdeDisk, "DMA Aborted before reading PRD entry\n");
        updateState(ACT_CMD_ERROR);
        return;
    }

    if (dmaState != Dma_Transfer || devState != Transfer_Data_Dma)
        panic("Inconsistent DMA transfer state: dmaState = %d devState = %d\n",
              dmaState, devState);

    if (ctrl->dmaPending() || ctrl->drainState() != DrainState::Running) {
        schedule(dmaTransferEvent, curTick() + DMA_BACKOFF_PERIOD);
        return;
    } else
        ctrl->dmaRead(curPrdAddr, sizeof(PrdEntry_t), &dmaPrdReadEvent,
                (uint8_t*)&curPrd.entry);
}

void
IdeDisk::dmaPrdReadDone()
{
    if (dmaAborted) {
        DPRINTF(IdeDisk, "DMA Aborted while reading PRD entry\n");
        updateState(ACT_CMD_ERROR);
        return;
    }
    

    DPRINTF(IdeDisk,
            "PRD: baseAddr:%#x (%#x) byteCount:%d (%d) eot:%#x sector:%d Address:%#x\n",
            curPrd.getBaseAddr(), pciToDma(curPrd.getBaseAddr()),
            curPrd.getByteCount(), (cmdBytesLeft/SectorSize),
            curPrd.getEOT(), curSector, (curSector * SectorSize));
    
    hil->SSDoperation((Addr) curSector , (curPrd.getByteCount()/SectorSize), curTick(), !dmaRead);
    /*
    if(SSDenable > 0) {
      hil.SSDoperation((curSector * SectorSize), (cmdBytesLeft/SectorSize), curTick(), dmaRead); //hil->SSDoperation((curSector * SectorSize), (cmdBytesLeft/SectorSize), curTick(), dmaRead); ///////////////////////////////////////////////Ahmed Abulila
      }*/
    // the prd pointer has already been translated, so just do an increment
    curPrdAddr = curPrdAddr + sizeof(PrdEntry_t);

    if (dmaRead) {
        doDmaDataRead();
	wrtBytes += curPrd.getByteCount();
    } else {
        doDmaDataWrite();
	rdBytes += curPrd.getByteCount();
    }
}

void
IdeDisk::doDmaDataRead()
{
    /** @todo we need to figure out what the delay actually will be */
///////////////////////////////////////////////Ahmed Abulila
//    Tick totalDiskDelay = diskDelay + (curPrd.getByteCount() / SectorSize);
    Tick totalDiskDelay;
    std::map<Addr, Tick>::iterator iDelay = hil->delayMap.find((Addr) curSector);
    if(iDelay == hil->delayMap.end()){
      printf("the address is %d\n",curSector);
      panic("Error: Flag Delay Fault\n");
    }
    
    totalDiskDelay = iDelay->second;//hil.getLatency(curTick());//hil->getLatency(curTick());
    iDelay->second = 0;
    statsUpdate();
    /*    if(SSDenable > 0) {
      totalDiskDelay = hil.getLatency(curTick());//hil->getLatency(curTick());
    } else {
      totalDiskDelay = diskDelay + (curPrd.getByteCount() / SectorSize);
      }*/
///////////////////////////////////////////////Ahmed Abulila
    DPRINTF(IdeDisk, "doDmaRead, diskDelay: %d totalDiskDelay: %d\n",
            diskDelay, totalDiskDelay);

    schedule(dmaReadWaitEvent, curTick() + totalDiskDelay);
}

void
IdeDisk::regStats()
{
    using namespace Stats;
    dmaReadFullPages
        .name(name() + ".dma_read_full_pages")
        .desc("Number of full page size DMA reads (not PRD).")
        ;
    dmaReadBytes
        .name(name() + ".dma_read_bytes")
        .desc("Number of bytes transfered via DMA reads (not PRD).")
        ;
    dmaReadTxs
        .name(name() + ".dma_read_txs")
        .desc("Number of DMA read transactions (not PRD).")
        ;

    dmaWriteFullPages
        .name(name() + ".dma_write_full_pages")
        .desc("Number of full page size DMA writes.")
        ;
    dmaWriteBytes
        .name(name() + ".dma_write_bytes")
        .desc("Number of bytes transfered via DMA writes.")
        ;
    dmaWriteTxs
        .name(name() + ".dma_write_txs")
        .desc("Number of DMA write transactions.")
        ;
    /*
    wrtPg
      .name("Disk.wrtPg")
      .desc("Total number of pages that have been evicted from the main memory (pages)");
    wrtPg = dmaReadFullPages;

    rdPg
      .name("Disk.rdPg")
      .desc("Total number of pages that have been read from the disk (pages)");
    rdPg = dmaWriteFullPages;
    */

    
    wrtBytes
      .name("Disk.wrtBytes")
      .desc("Total number of Bytes that have been written to the disk");
    //wrtPg = dmaReadFullPages;

    rdBytes
      .name("Disk.rdBytes")
      .desc("Total number of Bytes that have been read from the disk");
    //rd = dmaWriteFullPages;
    /*    
    wrtPg2
      .name("Disk.wrtPg2")
      .desc("Total number of pages that have been evicted from the main memory (pages)");
    wrtPg2 = wrtBytes/(SectorSize * 4);

    rdPg2
      .name("Disk.rdPg2")
      .desc("Total number of pages that have been read from the disk (pages)");
      rdPg2 = rdBytes/(SectorSize * 4);*/

    if(!SSDenable) {
      HDD_CAP_RD_AVG
	.name("HDD.CAP.RD.AVG")
	.desc("HDD layer average number of bytes of read operations (Bytes)");
                  
      HDD_CAP_RD_MIN
	.name("HDD.CAP.RD.MIN")
	.desc("HDD layer lowest number of bytes of read operations (Bytes)");
                                                                       
      HDD_CAP_RD_MAX                                                       
	.name("HDD.CAP.RD.MAX")                                      
	.desc("HDD layer largest number of bytes of read operations (Bytes)");
                                                                        
      HDD_CAP_RD_TOT                                                      
	.name("HDD.CAP.RD.TOT")                                       
	.desc("HDD layer total number of bytes of read operations (Bytes)");  
      
      HDD_CAP_RD_COUNT                                                    
	.name("HDD.CAP.RD.COUNT")                                     
	.desc("HDD layer count of read operations");                  
      
      HDD_CAP_WR_AVG                                                      
	.name("HDD.CAP.WR.AVG")                                       
	.desc("HDD layer average number of bytes of write operations (Bytes)");
                                                                         
      HDD_CAP_WR_MIN                                                       
	.name("HDD.CAP.WR.MIN")                                        
	.desc("HDD layer lowest number of bytes of write operations (Bytes)"); 
                                                                         
      HDD_CAP_WR_MAX                                                       
	.name("HDD.CAP.WR.MAX")                                        
	.desc("HDD layer largest number of bytes of write operations (Bytes)");
                                                                         
      HDD_CAP_WR_TOT                                                       
	.name("HDD.CAP.WR.TOT")                                        
	.desc("HDD layer total number of bytes of write operations (Bytes)"); 
    
      HDD_CAP_WR_COUNT                                                     
	.name("HDD.CAP.WR.COUNT")                                      
	.desc("HDD layer count of write operations");
      
      HDD_CAP_TOT_AVG                                                      
	.name("HDD.CAP.TOT.AVG")                                       
	.desc("HDD layer average number of bytes of all operations (Bytes)");
                                                                         
      HDD_CAP_TOT_MIN                                                      
	.name("HDD.CAP.TOT.MIN")                                       
	.desc("HDD layer lowest number of bytes of all operations (Bytes)");
                                                                         
      HDD_CAP_TOT_MAX                                                      
	.name("HDD.CAP.TOT.MAX")                                       
	.desc("HDD layer largest number of bytes of all operations (Bytes)");
                                                                         
      HDD_CAP_TOT_TOT                                                      
	.name("HDD.CAP.TOT.TOT")                                       
	.desc("HDD layer total number of bytes of all operations (Bytes)");
                                                                         
      HDD_CAP_TOT_COUNT                                                    
	.name("HDD.CAP.TOT.COUNT")                                     
	.desc("HDD layer count of all operations");
      
      HDD_BW_RD_AVG                                                        
	.name("HDD.BW.RD.WOIDLE.AVG")
	.desc("HDD layer average bandwidth of read operations MB/s (excluding device idle time)");
                                                                         
      HDD_BW_RD_MIN                                                        
	.name("HDD.BW.RD.WOIDLE.MIN")
	.desc("HDD layer lowest bandwidth of read operations MB/s (excluding device idle time)");
                                                                         
      HDD_BW_RD_MAX                                                        
	.name("HDD.BW.RD.WOIDLE.MAX")
	.desc("HDD layer largest bandwidth of read operations MB/s (excluding device idle time)");

      HDD_BW_RD_AVG_WIDLE
	.name("HDD.BW.RD.WIDLE.AVG")
	.desc("HDD layer average bandwidth of read operations MB/s (including device idle time)");

      HDD_BW_RD_MIN_WIDLE
	.name("HDD.BW.RD.WIDLE.MIN")
	.desc("HDD layer lowest bandwidth of read operations MB/s (including device idle time)");

      HDD_BW_RD_MAX_WIDLE
	.name("HDD.BW.RD.WIDLE.MAX")
	.desc("HDD layer largest bandwidth of read operations MB/s (including device idle time)");
      
      HDD_BW_RD_AVG_ONLY
	.name("HDD.BW.RD.ONLY.AVG")
	.desc("HDD layer average read-only bandwidth MB/s (excluding device idle time)");

      HDD_BW_RD_MIN_ONLY
	.name("HDD.BW.RD.ONLY.MIN")
	.desc("HDD layer lowest read-only bandwidth MB/s (excluding device idle time)");

      HDD_BW_RD_MAX_ONLY
	.name("HDD.BW.RD.ONLY.MAX")
	.desc("HDD layer largest read-only bandwidth MB/s (excluding device idle time)");

      HDD_BW_WR_AVG                                                        
	.name("HDD.BW.WR.WOIDLE.AVG")
	.desc("HDD layer average bandwidth of write operations MB/s (excluding device idle time)");
      
      HDD_BW_WR_MIN                                                        
	.name("HDD.BW.WR.WOIDLE.MIN")
	.desc("HDD layer lowest bandwidth of write operations MB/s (excluding device idle time)");
      
      HDD_BW_WR_MAX                                                        
	.name("HDD.BW.WR.WOIDLE.MAX")
	.desc("HDD layer largest bandwidth of write operations MB/s (excluding device idle time)");

      HDD_BW_WR_AVG_WIDLE
	.name("HDD.BW.WR.WIDLE.AVG")
	.desc("HDD layer average bandwidth of write operations MB/s (including device idle time)");

      HDD_BW_WR_MIN_WIDLE
	.name("HDD.BW.WR.WIDLE.MIN")
	.desc("HDD layer lowest bandwidth of write operations MB/s (including device idle time)");

      HDD_BW_WR_MAX_WIDLE
	.name("HDD.BW.WR.WIDLE.MAX")
	.desc("HDD layer largest bandwidth of write operations MB/s (including device idle time)");
      
      HDD_BW_WR_AVG_ONLY
	.name("HDD.BW.WR.ONLY.AVG")
	.desc("HDD layer average write-only bandwidth MB/s (excluding device idle time)");

      HDD_BW_WR_MIN_ONLY
	.name("HDD.BW.WR.ONLY.MIN")
	.desc("HDD layer lowest write-only bandwidth MB/s (excluding device idle time)");

      HDD_BW_WR_MAX_ONLY
	.name("HDD.BW.WR.ONLY.MAX")
	.desc("HDD layer largest write-only bandwidth MB/s (excluding device idle time)");

      HDD_LAT_RD_AVG                                                       
	.name("HDD.LAT.RD.AVG")                                        
	.desc("HDD layer average latency of read operations (us)");
      
      HDD_LAT_RD_MIN                                                       
	.name("HDD.LAT.RD.MIN")                                        
	.desc("HDD layer lowest latency of read operations (us)");
      
      HDD_LAT_RD_MAX                                                       
	.name("HDD.LAT.RD.MAX")                                        
	.desc("HDD layer largest latency of read operations (us)");
      
      HDD_LAT_WR_AVG                                                       
	.name("HDD.LAT.WR.AVG")                                        
	.desc("HDD layer average latency of write operations (us)");
      
      HDD_LAT_WR_MIN                
	.name("HDD.LAT.WR.MIN") 
	.desc("HDD layer lowest latency of write operations (us)");
      
      HDD_LAT_WR_MAX                
	.name("HDD.LAT.WR.MAX") 
	.desc("HDD layer largest latency of write operations (us)");
      
      HDD_IO_RD_AVG                 
	.name("HDD.IO.RD.WOIDLE.AVG")
	.desc("HDD layer average IOPS of read operations (excluding device idle time)");
      
      HDD_IO_RD_MIN                 
	.name("HDD.IO.RD.WOIDLE.MIN")
	.desc("HDD layer lowest IOPS of read operations (excluding device idle time)");
      
      HDD_IO_RD_MAX
	.name("HDD.IO.RD.WOIDLE.MAX")
	.desc("HDD layer largest IOPS of read operations (excluding device idle time)");

      HDD_IO_RD_AVG_WIDLE
	.name("HDD.IO.RD.WIDLE.AVG")
	.desc("HDD layer average IOPS of read operations (including device idle time)");

      HDD_IO_RD_MIN_WIDLE
	.name("HDD.IO.RD.WIDLE.MIN")
	.desc("HDD layer lowest IOPS of read operations (including device idle time)");

      HDD_IO_RD_MAX_WIDLE
	.name("HDD.IO.RD.WIDLE.MAX")
	.desc("HDD layer largest IOPS of read operations (including device idle time)");
      
      HDD_IO_RD_AVG_ONLY
	.name("HDD.IO.RD.ONLY.AVG")
	.desc("HDD layer average read-only IOPS (excluding device idle time)");

      HDD_IO_RD_MIN_ONLY
	.name("HDD.IO.RD.ONLY.MIN")
	.desc("HDD layer lowest read-only IOPS (excluding device idle time)");

      HDD_IO_RD_MAX_ONLY
	.name("HDD.IO.RD.ONLY.MAX")
	.desc("HDD layer largest read-only IOPS (excluding device idle time)");

      HDD_IO_WR_AVG
	.name("HDD.IO.WR.WOIDLE.AVG")
	.desc("HDD layer average IOPS of write operations (excluding device idle time)");
      
      HDD_IO_WR_MIN
	.name("HDD.IO.WR.WOIDLE.MIN")
	.desc("HDD layer lowest IOPS of write operations (excluding device idle time)");
      
      HDD_IO_WR_MAX
	.name("HDD.IO.WR.WOIDLE.MAX")
	.desc("HDD layer largest IOPS of write operations (excluding device idle time)");

      HDD_IO_WR_AVG_WIDLE
	.name("HDD.IO.WR.WIDLE.AVG")
	.desc("HDD layer average IOPS of write operations (including device idle time)");

      HDD_IO_WR_MIN_WIDLE
	.name("HDD.IO.WR.WIDLE.MIN")
	.desc("HDD layer lowest IOPS of write operations (including device idle time)");

      HDD_IO_WR_MAX_WIDLE
	.name("HDD.IO.WR.WIDLE.MAX")
	.desc("HDD layer largest IOPS of write operations (including device idle time)");

      HDD_IO_WR_AVG_ONLY
	.name("HDD.IO.WR.ONLY.AVG")
	.desc("HDD layer average write-only IOPS (excluding device idle time)");

      HDD_IO_WR_MIN_ONLY
	.name("HDD.IO.WR.ONLY.MIN")
	.desc("HDD layer lowest write-only IOPS (excluding device idle time)");

      HDD_IO_WR_MAX_ONLY
	.name("HDD.IO.WR.ONLY.MAX")
	.desc("HDD layer largest write-only IOPS (excluding device idle time)");

      HDD_DEVICE_IDLE
	  .name("HDD.DEVICE.IDLETIME")
	  .desc("HDD layer device idle time (ms)");

      HDD_DEVICE_BUSY
	  .name("HDD.DEVICE.BUSYTIME")
	  .desc("HDD layer device busy time (ms)");

    } else {
      PAL_CAP_RD_AVG
	.name("SSD.PAL.CAP.RD.AVG")
	.desc("SSD PAL layer average number of bytes of read operations (Bytes)");
      
      PAL_CAP_RD_MIN
	.name("SSD.PAL.CAP.RD.MIN")
	.desc("SSD PAL layer lowest number of bytes of read operations (Bytes)");
      
      PAL_CAP_RD_MAX
	.name("SSD.PAL.CAP.RD.MAX")
	.desc("SSD PAL layer largest number of bytes of read operations (Bytes)");
      
      PAL_CAP_RD_TOT
	.name("SSD.PAL.CAP.RD.TOT")
	.desc("SSD PAL layer total number of bytes of read operations (Bytes)");
      
      PAL_CAP_RD_COUNT
	.name("SSD.PAL.CAP.RD.COUNT")
	.desc("SSD PAL layer count of read operations");

      PAL_CAP_WR_AVG
	.name("SSD.PAL.CAP.WR.AVG")
	.desc("SSD PAL layer average number of bytes of write operations (Bytes)");
      
      PAL_CAP_WR_MIN
	.name("SSD.PAL.CAP.WR.MIN")
	.desc("SSD PAL layer lowest number of bytes of write operations (Bytes)");
      
      PAL_CAP_WR_MAX
	.name("SSD.PAL.CAP.WR.MAX")
	.desc("SSD PAL layer largest number of bytes of write operations (Bytes)");
      
      PAL_CAP_WR_TOT
	.name("SSD.PAL.CAP.WR.TOT")
	.desc("SSD PAL layer total number of bytes of write operations (Bytes)");
      
      PAL_CAP_WR_COUNT
	.name("SSD.PAL.CAP.WR.COUNT")
	.desc("SSD PAL layer count of write operations");
      
      PAL_CAP_ER_AVG
	.name("SSD.PAL.CAP.ER.AVG")
	.desc("SSD PAL layer average number of bytes of erase operations (Bytes)");
      
      PAL_CAP_ER_MIN
	.name("SSD.PAL.CAP.ER.MIN")
	.desc("SSD PAL layer lowest number of bytes of erase operations (Bytes)");

      PAL_CAP_ER_MAX
	.name("SSD.PAL.CAP.ER.MAX")
	.desc("SSD PAL layer largest number of bytes of erase operations (Bytes)");
      
      PAL_CAP_ER_TOT
	.name("SSD.PAL.CAP.ER.TOT")
	.desc("SSD PAL layer total number of bytes of erase operations (Bytes)");
      
      PAL_CAP_ER_COUNT
	.name("SSD.PAL.CAP.ER.COUNT")
	.desc("SSD PAL layer count of erase operations");
      
      
      PAL_CAP_TOT_AVG
	.name("SSD.PAL.CAP.TOT.AVG")
	.desc("SSD PAL layer average number of bytes of all operations (Bytes)");
      
      PAL_CAP_TOT_MIN
	.name("SSD.PAL.CAP.TOT.MIN")
	.desc("SSD PAL layer lowest number of bytes of all operations (Bytes)");

      PAL_CAP_TOT_MAX
	.name("SSD.PAL.CAP.TOT.MAX")
	.desc("SSD PAL layer largest number of bytes of all operations (Bytes)");

      PAL_CAP_TOT_TOT
	.name("SSD.PAL.CAP.TOT.TOT")
	.desc("SSD PAL layer total number of bytes of all operations (Bytes)");
      
      PAL_CAP_TOT_COUNT
	.name("SSD.PAL.CAP.TOT.COUNT")
	.desc("SSD PAL layer count of all operations");
      
      PAL_BW_RD_AVG
	.name("SSD.PAL.BW.RD.WOIDLE.AVG")
	.desc("SSD PAL layer average bandwidth of read operations (MB/s) (excluding device idle time)");
      
      PAL_BW_RD_MIN
	.name("SSD.PAL.BW.RD.WOIDLE.MIN")
	.desc("SSD PAL layer lowest bandwidth of read operations (MB/s) (excluding device idle time)");
      
      PAL_BW_RD_MAX
	.name("SSD.PAL.BW.RD.WOIDLE.MAX")
	.desc("SSD PAL layer largest bandwidth of read operations (MB/s) (excluding device idle time)");

      PAL_BW_RD_AVG_WIDLE
	.name("SSD.PAL.BW.RD.WIDLE.AVG")
	.desc("SSD PAL layer average bandwidth of read operations (MB/s) (including device idle time)");

      PAL_BW_RD_MIN_WIDLE
	.name("SSD.PAL.BW.RD.WIDLE.MIN")
	.desc("SSD PAL layer lowest bandwidth of read operations (MB/s) (including device idle time)");

      PAL_BW_RD_MAX_WIDLE
	.name("SSD.PAL.BW.RD.WIDLE.MAX")
	.desc("SSD PAL layer largest bandwidth of read operations (MB/s) (including device idle time)");
      
      PAL_BW_RD_AVG_ONLY
	.name("SSD.PAL.BW.RD.ONLY.AVG")
	.desc("SSD PAL layer average read-only bandwidth (MB/s) (excluding device idle time)");

      PAL_BW_RD_MIN_ONLY
	.name("SSD.PAL.BW.RD.ONLY.MIN")
	.desc("SSD PAL layer lowest read-only bandwidth (MB/s) (excluding device idle time)");

      PAL_BW_RD_MAX_ONLY
	.name("SSD.PAL.BW.RD.ONLY.MAX")
	.desc("SSD PAL layer largest read-only bandwidth (MB/s) (excluding device idle time)");

      PAL_BW_WR_AVG
	.name("SSD.PAL.BW.WR.WOIDLE.AVG")
	.desc("SSD PAL layer average bandwidth of write operations (MB/s) (excluding device idle time)");
      
      PAL_BW_WR_MIN
	.name("SSD.PAL.BW.WR.WOIDLE.MIN")
	.desc("SSD PAL layer lowest bandwidth of write operations (MB/s) (excluding device idle time)");

      PAL_BW_WR_MAX
	.name("SSD.PAL.BW.WR.WOIDLE.MAX")
	.desc("SSD PAL layer largest bandwidth of write operations (MB/s) (excluding device idle time)");

      PAL_BW_WR_AVG_WIDLE
	.name("SSD.PAL.BW.WR.WIDLE.AVG")
	.desc("SSD PAL layer average bandwidth of write operations (MB/s) (including device idle time)");

      PAL_BW_WR_MIN_WIDLE
	.name("SSD.PAL.BW.WR.WIDLE.MIN")
	.desc("SSD PAL layer lowest bandwidth of write operations (MB/s) (including device idle time)");

      PAL_BW_WR_MAX_WIDLE
	.name("SSD.PAL.BW.WR.WIDLE.MAX")
	.desc("SSD PAL layer largest bandwidth of write operations (MB/s) (including device idle time)");
      
      PAL_BW_WR_AVG_ONLY
	.name("SSD.PAL.BW.WR.ONLY.AVG")
	.desc("SSD PAL layer average write-only bandwidth (MB/s) (excluding device idle time)");

      PAL_BW_WR_MIN_ONLY
	.name("SSD.PAL.BW.WR.ONLY.MIN")
	.desc("SSD PAL layer lowest write-only bandwidth (MB/s) (excluding device idle time)");

      PAL_BW_WR_MAX_ONLY
	.name("SSD.PAL.BW.WR.ONLY.MAX")
	.desc("SSD PAL layer largest write-only bandwidth (MB/s) (excluding device idle time)");

      PAL_BW_ER_AVG
	.name("SSD.PAL.BW.ER.WOIDLE.AVG")
	.desc("SSD PAL layer average bandwidth of erase operations (MB/s) (excluding device idle time)");
      
      PAL_BW_ER_MIN
	.name("SSD.PAL.BW.ER.WOIDLE.MIN")
	.desc("SSD PAL layer lowest bandwidth of erase operations (MB/s) (excluding device idle time)");
      
      PAL_BW_ER_MAX
	.name("SSD.PAL.BW.ER.WOIDLE.MAX")
	.desc("SSD PAL layer largest bandwidth of erase operations (MB/s) (excluding device idle time)");

      PAL_BW_ER_AVG_WIDLE
	.name("SSD.PAL.BW.ER.WIDLE.AVG")
	.desc("SSD PAL layer average bandwidth of erase operations (MB/s) (including device idle time)");

      PAL_BW_ER_MIN_WIDLE
	.name("SSD.PAL.BW.ER.WIDLE.MIN")
	.desc("SSD PAL layer lowest bandwidth of erase operations (MB/s) (including device idle time)");

      PAL_BW_ER_MAX_WIDLE
	.name("SSD.PAL.BW.ER.WIDLE.MAX")
	.desc("SSD PAL layer largest bandwidth of erase operations (MB/s) (including device idle time)");
      
      PAL_BW_ER_AVG_ONLY
	.name("SSD.PAL.BW.ER.ONLY.AVG")
	.desc("SSD PAL layer average erase-only bandwidth (MB/s) (excluding device idle time)");

      PAL_BW_ER_MIN_ONLY
	.name("SSD.PAL.BW.ER.ONLY.MIN")
	.desc("SSD PAL layer lowest erase-only bandwidth (MB/s) (excluding device idle time)");

      PAL_BW_ER_MAX_ONLY
	.name("SSD.PAL.BW.ER.ONLY.MAX")
	.desc("SSD PAL layer largest erase-only bandwidth (MB/s) (excluding device idle time)");
      
      PAL_BW_TOT_AVG
	.name("SSD.PAL.BW.TOT.WOIDLE.AVG")
	.desc("SSD PAL layer average bandwidth of all operations (MB/s) (excluding device idle time)");
      
      PAL_BW_TOT_MIN
	.name("SSD.PAL.BW.TOT.WOIDLE.MIN")
	.desc("SSD PAL layer lowest bandwidth of all operations (MB/s) (excluding device idle time)");
      
      PAL_BW_TOT_MAX
	.name("SSD.PAL.BW.TOT.WOIDLE.MAX")
	.desc("SSD PAL layer largest bandwidth of all operations (MB/s) (excluding device idle time)");

      PAL_BW_TOT_AVG_WIDLE
 	.name("SSD.PAL.BW.TOT.WIDLE.AVG")
 	.desc("SSD PAL layer average bandwidth of all operations (MB/s) (including device idle time)");

       PAL_BW_TOT_MIN_WIDLE
 	.name("SSD.PAL.BW.TOT.WIDLE.MIN")
 	.desc("SSD PAL layer lowest bandwidth of all operations (MB/s) (including device idle time)");

       PAL_BW_TOT_MAX_WIDLE
 	.name("SSD.PAL.BW.TOT.WIDLE.MAX")
 	.desc("SSD PAL layer largest bandwidth of all operations (MB/s) (including device idle time)");

      PAL_IO_RD_AVG
	.name("SSD.PAL.IO.RD.WOIDLE.AVG")
	.desc("SSD PAL layer average IOPS of read operations (excluding device idle time)");
      
      PAL_IO_RD_MIN
	.name("SSD.PAL.IO.RD.WOIDLE.MIN")
	.desc("SSD PAL layer lowest IOPS of read operations (excluding device idle time)");
      
      PAL_IO_RD_MAX
	.name("SSD.PAL.IO.RD.WOIDLE.MAX")
	.desc("SSD PAL layer largest IOPS of read operations (excluding device idle time)");

      PAL_IO_RD_AVG_WIDLE
	.name("SSD.PAL.IO.RD.WIDLE.AVG")
	.desc("SSD PAL layer average IOPS of read operations (including device idle time)");

      PAL_IO_RD_MIN_WIDLE
	.name("SSD.PAL.IO.RD.WIDLE.MIN")
	.desc("SSD PAL layer lowest IOPS of read operations (including device idle time)");

      PAL_IO_RD_MAX_WIDLE
	.name("SSD.PAL.IO.RD.WIDLE.MAX")
	.desc("SSD PAL layer largest IOPS of read operations (including device idle time)");

      PAL_IO_RD_AVG_ONLY
	.name("SSD.PAL.IO.RD.ONLY.AVG")
	.desc("SSD PAL layer average read-only IOPS (excluding device idle time)");

      PAL_IO_RD_MIN_ONLY
	.name("SSD.PAL.IO.RD.ONLY.MIN")
	.desc("SSD PAL layer lowest read-only IOPS (excluding device idle time)");

      PAL_IO_RD_MAX_ONLY
	.name("SSD.PAL.IO.RD.ONLY.MAX")
	.desc("SSD PAL layer largest read-only IOPS (excluding device idle time)");

      PAL_IO_WR_AVG
	.name("SSD.PAL.IO.WR.WOIDLE.AVG")
	.desc("SSD PAL layer average IOPS of write operations (excluding device idle time)");
      
      PAL_IO_WR_MIN
	.name("SSD.PAL.IO.WR.WOIDLE.MIN")
	.desc("SSD PAL layer lowest IOPS of write operations (excluding device idle time)");
      
      PAL_IO_WR_MAX
	.name("SSD.PAL.IO.WR.WOIDLE.MAX")
	.desc("SSD PAL layer largest IOPS of write operations (excluding device idle time)");

      PAL_IO_WR_AVG_WIDLE
 	.name("SSD.PAL.IO.WR.WIDLE.AVG")
 	.desc("SSD PAL layer average IOPS of write operations (including device idle time)");

       PAL_IO_WR_MIN_WIDLE
 	.name("SSD.PAL.IO.WR.WIDLE.MIN")
 	.desc("SSD PAL layer lowest IOPS of write operations (including device idle time)");

       PAL_IO_WR_MAX_WIDLE
 	.name("SSD.PAL.IO.WR.WIDLE.MAX")
 	.desc("SSD PAL layer largest IOPS of write operations (including device idle time)");

       PAL_IO_WR_AVG_ONLY
  	.name("SSD.PAL.IO.WR.ONLY.AVG")
  	.desc("SSD PAL layer average write-only IOPS (excluding device idle time)");

        PAL_IO_WR_MIN_ONLY
  	.name("SSD.PAL.IO.WR.ONLY.MIN")
  	.desc("SSD PAL layer lowest write-only IOPS (excluding device idle time)");

        PAL_IO_WR_MAX_ONLY
  	.name("SSD.PAL.IO.WR.ONLY.MAX")
  	.desc("SSD PAL layer largest write-only IOPS (excluding device idle time)");

      PAL_IO_ER_AVG
	.name("SSD.PAL.IO.ER.WOIDLE.AVG")
	.desc("SSD PAL layer average IOPS of erase operations (excluding device idle time)");

      PAL_IO_ER_MIN
	.name("SSD.PAL.IO.ER.WOIDLE.MIN")
	.desc("SSD PAL layer lowest IOPS of erase operations (excluding device idle time)");
      
      PAL_IO_ER_MAX
	.name("SSD.PAL.IO.ER.WOIDLE.MAX")
	.desc("SSD PAL layer largest IOPS of erase operations (excluding device idle time)");

      PAL_IO_ER_AVG_WIDLE
	.name("SSD.PAL.IO.ER.WIDLE.AVG")
	.desc("SSD PAL layer average IOPS of erase operations (including device idle time)");

      PAL_IO_ER_MIN_WIDLE
	.name("SSD.PAL.IO.ER.WIDLE.MIN")
	.desc("SSD PAL layer lowest IOPS of erase operations (including device idle time)");

      PAL_IO_ER_MAX_WIDLE
	.name("SSD.PAL.IO.ER.WIDLE.MAX")
	.desc("SSD PAL layer largest IOPS of erase operations (including device idle time)");

      PAL_IO_ER_AVG_ONLY
	.name("SSD.PAL.IO.ER.ONLY.AVG")
	.desc("SSD PAL layer average erase-only IOPS (excluding device idle time)");

      PAL_IO_ER_MIN_ONLY
	.name("SSD.PAL.IO.ER.ONLY.MIN")
	.desc("SSD PAL layer lowest erase-only IOPS (excluding device idle time)");

      PAL_IO_ER_MAX_ONLY
	.name("SSD.PAL.IO.ER.ONLY.MAX")
	.desc("SSD PAL layer largest erase-only IOPS (excluding device idle time)");
    
      PAL_IO_TOT_AVG
	.name("SSD.PAL.IO.TOT.WOIDLE.AVG")
	.desc("SSD PAL layer average IOPS of all operations (excluding device idle time)");

      PAL_IO_TOT_MIN
	.name("SSD.PAL.IO.TOT.WOIDLE.MIN")
	.desc("SSD PAL layer lowest IOPS of all operations (excluding device idle time)");

      PAL_IO_TOT_MAX
	.name("SSD.PAL.IO.TOT.WOIDLE.MAX")
	.desc("SSD PAL layer largest IOPS of all operations (excluding device idle time)");

      PAL_IO_TOT_AVG_WIDLE
	.name("SSD.PAL.IO.TOT.WIDLE.AVG")
	.desc("SSD PAL layer average IOPS of all operations (including device idle time)");

      PAL_IO_TOT_MIN_WIDLE
	.name("SSD.PAL.IO.TOT.WIDLE.MIN")
	.desc("SSD PAL layer lowest IOPS of all operations (including device idle time)");

      PAL_IO_TOT_MAX_WIDLE
	.name("SSD.PAL.IO.TOT.WIDLE.MAX")
	.desc("SSD PAL layer largest IOPS of all operations (including device idle time)");
      
////////////////////////////////////
      PAL_LAT_RD_AVG
	.name("SSD.PAL.LAT.RD.AVG")
	.desc("SSD PAL layer average LATENCY of read operations(us)");
      
      PAL_LAT_RD_MIN
	.name("SSD.PAL.LAT.RD.MIN")
	.desc("SSD PAL layer lowest LATENCY of read operations(us)");
      
      PAL_LAT_RD_MAX
	.name("SSD.PAL.LAT.RD.MAX")
	.desc("SSD PAL layer largest LATENCY of read operations(us)");

      PAL_LAT_WR_AVG
	.name("SSD.PAL.LAT.WR.AVG")
	.desc("SSD PAL layer average LATENCY of write operations(us)");
      
      PAL_LAT_WR_MIN
	.name("SSD.PAL.LAT.WR.MIN")
	.desc("SSD PAL layer lowest LATENCY of write operations(us)");
      
      PAL_LAT_WR_MAX
	.name("SSD.PAL.LAT.WR.MAX")
	.desc("SSD PAL layer largest LATENCY of write operations(us)");
      
      PAL_LAT_ER_AVG
	.name("SSD.PAL.LAT.ER.AVG")
	.desc("SSD PAL layer average LATENCY of erase operations(us)");
      
      PAL_LAT_ER_MIN
	.name("SSD.PAL.LAT.ER.MIN")
	.desc("SSD PAL layer lowest LATENCY of erase operations(us)");
      
      PAL_LAT_ER_MAX
	.name("SSD.PAL.LAT.ER.MAX")
	.desc("SSD PAL layer largest LATENCY of erase operations(us)");
      
      PAL_LAT_TOT_AVG
	.name("SSD.PAL.LAT.TOT.AVG")
	.desc("SSD PAL layer average LATENCY of all operations(us)");
      
      PAL_LAT_TOT_MIN
	.name("SSD.PAL.LAT.TOT.MIN")
	.desc("SSD PAL layer lowest LATENCY of all operations(us)");
      
      PAL_LAT_TOT_MAX
	.name("SSD.PAL.LAT.TOT.MAX")
	.desc("SSD PAL layer largest LATENCY of all operations(us)");

      PAL_DEVICE_IDLE
	.name("SSD.PAL.DEVICE.IDLETIME")
	.desc("SSD PAL layer device idle time (ms)");

      PAL_DEVICE_BUSY
	.name("SSD.PAL.DEVICE.BUSYTIME")
	.desc("SSD PAL layer device busy time (ms)");

//////////////////////////////////


      FTL_HOST_CAP_RD_TOT
	.name("SSD.FTL.HOST.CAP.RD.TOT")
	.desc("SSD FTL HOST layer total volume of read operations (MB)");



      FTL_HOST_CAP_RD_COUNT
	.name("SSD.FTL.HOST.CAP.RD.COUNT")
	.desc("SSD FTL HOST layer count of read operations");
      
      
      FTL_HOST_CAP_WR_TOT
	.name("SSD.FTL.HOST.CAP.WR.TOT")
	.desc("SSD FTL HOST layer total volume of of write operations (MB)");
      
      
      FTL_HOST_CAP_WR_COUNT
	.name("SSD.FTL.HOST.CAP.WR.COUNT")
	.desc("SSD FTL HOST layer count of write operations");
      
      
      
      FTL_HOST_BW_RD_AVG
	.name("SSD.FTL.HOST.BW.RD.AVG")
	.desc("SSD FTL HOST layer average bandwidth of read operations (MBps)");
      
      FTL_HOST_BW_RD_MIN
	.name("SSD.FTL.HOST.BW.RD.MIN")
	.desc("SSD FTL HOST layer lowest bandwidth of read operations (MBps)");
     
      FTL_HOST_BW_RD_MAX
	.name("SSD.FTL.HOST.BW.RD.MAX")
	.desc("SSD FTL HOST layer largest bandwidth of read operations (MBps)");
      
      FTL_HOST_BW_WR_AVG
	.name("SSD.FTL.HOST.BW.WR.AVG")
	.desc("SSD FTL HOST layer average bandwidth of write operations (MBps)");
      
      FTL_HOST_BW_WR_MIN
	.name("SSD.FTL.HOST.BW.WR.MIN")
	.desc("SSD FTL HOST layer lowest bandwidth of write operations (MBps)");
      
      FTL_HOST_BW_WR_MAX
	.name("SSD.FTL.HOST.BW.WR.MAX")
	.desc("SSD FTL HOST layer largest bandwidth of write operations (MBps)");

       FTL_HOST_BW_TOT_AVG
	.name("SSD.FTL.HOST.BW.TOT.AVG")
	.desc("SSD FTL HOST layer average bandwidth of all operations (MBps)");
      
      FTL_HOST_BW_TOT_MIN
	.name("SSD.FTL.HOST.BW.TOT.MIN")
	.desc("SSD FTL HOST layer lowest bandwidth of all operations (MBps)");
     
      FTL_HOST_BW_TOT_MAX
	.name("SSD.FTL.HOST.BW.TOT.MAX")
	.desc("SSD FTL HOST layer largest bandwidth of all operations (MBps)");
      
      FTL_HOST_BW_TOT_AVG_WIDLE
	.name("SSD.FTL.HOST.BW.TOT.AVG.WIDLE")
	.desc("SSD FTL HOST layer average bandwidth of all operations, considering idle time (MBps)");
      
      FTL_HOST_BW_TOT_MIN_WIDLE
	.name("SSD.FTL.HOST.BW.TOT.MIN.WIDLE")
	.desc("SSD FTL HOST layer lowest bandwidth of all operations, considering idle time (MBps)");
      
      FTL_HOST_BW_TOT_MAX_WIDLE
	.name("SSD.FTL.HOST.BW.TOT.MAX.WIDLE")
	.desc("SSD FTL HOST layer largest bandwidth of all operations, considering idle time (MBps)");

      
      FTL_HOST_LAT_RD_AVG
	.name("SSD.FTL.HOST.LAT.RD.AVG")
	.desc("SSD FTL HOST layer average latency of read operations (us)");

      FTL_HOST_LAT_RD_MIN
	.name("SSD.FTL.HOST.LAT.RD.MIN")
	.desc("SSD FTL HOST layer lowest latency of read operations (us)");
      
      FTL_HOST_LAT_RD_MAX
	.name("SSD.FTL.HOST.LAT.RD.MAX")
	.desc("SSD FTL HOST layer largest latency of read operations (us)");
      
      FTL_HOST_LAT_WR_AVG
	.name("SSD.FTL.HOST.LAT.WR.AVG")
	.desc("SSD FTL HOST layer average latency of write operations (us)");
      
      FTL_HOST_LAT_WR_MIN
	.name("SSD.FTL.HOST.LAT.WR.MIN")
	.desc("SSD FTL HOST layer lowest latency of write operations (us)");
      
      FTL_HOST_LAT_WR_MAX
	.name("SSD.FTL.HOST.LAT.WR.MAX")
	.desc("SSD FTL HOST layer largest latency of write operations (us)");
      
      
      FTL_HOST_IO_RD_AVG
	.name("SSD.FTL.HOST.IO.RD.AVG")
	.desc("SSD FTL HOST layer average IOPS of read operations");
      
      FTL_HOST_IO_RD_MIN
	.name("SSD.FTL.HOST.IO.RD.MIN")
	.desc("SSD FTL HOST layer lowest IOPS of read operations");
    
      FTL_HOST_IO_RD_MAX
	.name("SSD.FTL.HOST.IO.RD.MAX")
	.desc("SSD FTL HOST layer largest IOPS of read operations");
      
      FTL_HOST_IO_WR_AVG
	.name("SSD.FTL.HOST.IO.WR.AVG")
	.desc("SSD FTL HOST layer average IOPS of write operations");
      
      FTL_HOST_IO_WR_MIN
	.name("SSD.FTL.HOST.IO.WR.MIN")
	.desc("SSD FTL HOST layer lowest IOPS of write operations");
      
      FTL_HOST_IO_WR_MAX
	.name("SSD.FTL.HOST.IO.WR.MAX")
	.desc("SSD FTL HOST layer largest IOPS of write operations");
      
       FTL_HOST_IO_TOT_AVG
	.name("SSD.FTL.HOST.IO.TOT.AVG")
	.desc("SSD FTL HOST layer average IOPS of all operations");
      
      FTL_HOST_IO_TOT_MIN
	.name("SSD.FTL.HOST.IO.TOT.MIN")
	.desc("SSD FTL HOST layer lowest IOPS of all operations");
    
      FTL_HOST_IO_TOT_MAX
	.name("SSD.FTL.HOST.IO.TOT.MAX")
	.desc("SSD FTL HOST layer largest IOPS of all operations");
      
      FTL_HOST_IO_TOT_AVG_WIDLE
	.name("SSD.FTL.HOST.IO.TOT.AVG.WIDLE")
	.desc("SSD FTL HOST layer average IOPS of all operations, considering idle times");
      
      FTL_HOST_IO_TOT_MIN_WIDLE
	.name("SSD.FTL.HOST.IO.TOT.MIN.WIDLE")
	.desc("SSD FTL HOST layer lowest IOPS of all operations, considering idle times");
      
      FTL_HOST_IO_TOT_MAX_WIDLE
	.name("SSD.FTL.HOST.IO.TOT.MAX.WIDLE")
	.desc("SSD FTL HOST layer largest IOPS of all operations, considering idle times");
      
      
      FTL_HOST_SIZE_RD_AVG
	.name("SSD.FTL.HOST.SIZE.RD.AVG")
	.desc("SSD FTL HOST layer average size of read operations (KB)");
      
      FTL_HOST_SIZE_RD_MIN
	.name("SSD.FTL.HOST.SIZE.RD.MIN")
	.desc("SSD FTL HOST layer lowest size of read operations (KB)");

      FTL_HOST_SIZE_RD_MAX
	.name("SSD.FTL.HOST.SIZE.RD.MAX")
	.desc("SSD FTL HOST layer largest size of read operations (KB)");
      
      FTL_HOST_SIZE_WR_AVG
	.name("SSD.FTL.HOST.SIZE.WR.AVG")
	.desc("SSD FTL HOST layer average size of write operations (KB)");
      
      FTL_HOST_SIZE_WR_MIN
	.name("SSD.FTL.HOST.SIZE.WR.MIN")
	.desc("SSD FTL HOST layer lowest size of write operations (KB)");
      
      FTL_HOST_SIZE_WR_MAX
	.name("SSD.FTL.HOST.SIZE.WR.MAX")
	.desc("SSD FTL HOST layer largest size of write operations (KB)");

      
      FTL_PAL_CAP_RD_TOT
	.name("SSD.FTL.PAL.CAP.RD.TOT")
	.desc("SSD FTL PAL layer total number of bytes of read operations (MB)");
      
      FTL_PAL_CAP_RD_COUNT
	.name("SSD.FTL.PAL.CAP.RD.COUNT")
	.desc("SSD FTL PAL layer count of read operations");
      
      FTL_PAL_CAP_WR_TOT
	.name("SSD.FTL.PAL.CAP.WR.TOT")
	.desc("SSD FTL PAL layer total number of bytes of write operations (MB)");
      
      FTL_PAL_CAP_WR_COUNT
	.name("SSD.FTL.PAL.CAP.WR.COUNT")
	.desc("SSD FTL PAL layer count of write operations");
      
      FTL_PAL_CAP_ER_COUNT
	.name("SSD.FTL.PAL.CAP.ER.COUNT")
	.desc("SSD FTL PAL layer count of erase operations");
      
      FTL_PAL_LAT_RD_AVG
	.name("SSD.FTL.PAL.LAT.RD.AVG")
	.desc("SSD FTL PAL layer average latency of read operations (us)");
      
      FTL_PAL_LAT_RD_MIN
	.name("SSD.FTL.PAL.LAT.RD.MIN")
	.desc("SSD FTL PAL layer lowest latency of read operations (us)");
      
      FTL_PAL_LAT_RD_MAX
	.name("SSD.FTL.PAL.LAT.RD.MAX")
	.desc("SSD FTL PAL layer largest latency of read operations (us)");
      
      FTL_PAL_LAT_WR_AVG
	.name("SSD.FTL.PAL.LAT.WR.AVG")
	.desc("SSD FTL PAL layer average latency of write operations (us)");
      
      FTL_PAL_LAT_WR_MIN
	.name("SSD.FTL.PAL.LAT.WR.MIN")
	.desc("SSD FTL PAL layer lowest latency of write operations (us)");
      
      FTL_PAL_LAT_WR_MAX
	.name("SSD.FTL.PAL.LAT.WR.MAX")
	.desc("SSD FTL PAL layer largest latency of write operations (us)");
      
      FTL_PAL_LAT_ER_AVG
	.name("SSD.FTL.PAL.LAT.ER.AVG")
	.desc("SSD FTL PAL layer average latency of erase operations (us)");
      
      FTL_PAL_LAT_ER_MIN
	.name("SSD.FTL.PAL.LAT.ER.MIN")
	.desc("SSD FTL PAL layer lowest latency of erase operations (us)");

      FTL_PAL_LAT_ER_MAX
	.name("SSD.FTL.PAL.LAT.ER.MAX")
	.desc("SSD FTL PAL layer largest latency of erase operations (us)");

      FTL_MAP_CAP_GC_COUNT
	.name("SSD.FTL.MAP.CAP.GC.COUNT")
	.desc("SSD FTL MAP layer count of GC operations");

      FTL_MAP_CAP_ER_COUNT
	.name("SSD.FTL.MAP.CAP.ER.COUNT")
	.desc("SSD FTL MAP layer count of erase operations");

      FTL_MAP_LAT_GC_AVG
	.name("SSD.FTL.MAP.LAT.GC.AVG")
	.desc("SSD FTL MAP layer average latency of GC operations (us)");
      
      FTL_MAP_LAT_GC_MIN
	.name("SSD.FTL.MAP.LAT.GC.MIN")
	.desc("SSD FTL MAP layer lowest latency of GC operations (us)");

      FTL_MAP_LAT_GC_MAX
	.name("SSD.FTL.MAP.LAT.GC.MAX")
	.desc("SSD FTL MAP layer largest latency of GC operations (us)");
    }
}

void
IdeDisk::doDmaRead()
{
    if (dmaAborted) {
        DPRINTF(IdeDisk, "DMA Aborted in middle of Dma Read\n");
        if (dmaReadCG)
            delete dmaReadCG;
        dmaReadCG = NULL;
        updateState(ACT_CMD_ERROR);
        return;
    }

    if (!dmaReadCG) {
        // clear out the data buffer
        memset(dataBuffer, 0, MAX_DMA_SIZE);
        dmaReadCG = new ChunkGenerator(curPrd.getBaseAddr(),
                curPrd.getByteCount(), TheISA::PageBytes);

    }
    if (ctrl->dmaPending() || ctrl->drainState() != DrainState::Running) {
        schedule(dmaReadWaitEvent, curTick() + DMA_BACKOFF_PERIOD);
        return;
    } else if (!dmaReadCG->done()) {
        assert(dmaReadCG->complete() < MAX_DMA_SIZE);
        ctrl->dmaRead(pciToDma(dmaReadCG->addr()), dmaReadCG->size(),
                &dmaReadWaitEvent, dataBuffer + dmaReadCG->complete());
        dmaReadBytes += dmaReadCG->size();
        dmaReadTxs++;
        if (dmaReadCG->size() == TheISA::PageBytes)
            dmaReadFullPages++;
        dmaReadCG->next();
    } else {
        assert(dmaReadCG->done());
        delete dmaReadCG;
        dmaReadCG = NULL;
        dmaReadDone();
    }
}

void
IdeDisk::dmaReadDone()
{
    uint32_t bytesWritten = 0;

    // write the data to the disk image
    for (bytesWritten = 0; bytesWritten < curPrd.getByteCount();
         bytesWritten += SectorSize) {

        cmdBytesLeft -= SectorSize;
        writeDisk(curSector++, (uint8_t *)(dataBuffer + bytesWritten));
    }

    // check for the EOT
    if (curPrd.getEOT()) {
        assert(cmdBytesLeft == 0);
        dmaState = Dma_Idle;
        updateState(ACT_DMA_DONE);
    } else {
        doDmaTransfer();
    }
}

void
IdeDisk::doDmaDataWrite()
{
    /** @todo we need to figure out what the delay actually will be */
///////////////////////////////////////////////Ahmed Abulila
//    Tick totalDiskDelay = diskDelay + (curPrd.getByteCount() / SectorSize);
    Tick totalDiskDelay;
    std::map<Addr, Tick>::iterator iDelay = hil->delayMap.find((Addr) curSector);
    if(iDelay == hil->delayMap.end()){
      printf("the address is %d\n",curSector);
      panic("Error: Flag Delay Fault\n");
    }
    
    totalDiskDelay = iDelay->second;
    iDelay->second = 0;
    statsUpdate();
    /*if(SSDenable > 0) {
      totalDiskDelay = hil.getLatency(curTick());//hil->getLatency(curTick());
    } else {
      totalDiskDelay = diskDelay + (curPrd.getByteCount() / SectorSize);
      }*/
    //Tick totalDiskDelay = hil->getLatency(curTick());
///////////////////////////////////////////////Ahmed Abulila
    uint32_t bytesRead = 0;

    DPRINTF(IdeDisk, "doDmaWrite, diskDelay: %d totalDiskDelay: %d\n",
            diskDelay, totalDiskDelay);

    memset(dataBuffer, 0, MAX_DMA_SIZE);
    assert(cmdBytesLeft <= MAX_DMA_SIZE);
    while (bytesRead < curPrd.getByteCount()) {
        readDisk(curSector++, (uint8_t *)(dataBuffer + bytesRead));
        bytesRead += SectorSize;
        cmdBytesLeft -= SectorSize;
    }
    DPRINTF(IdeDisk, "doDmaWrite, bytesRead: %d cmdBytesLeft: %d\n",
            bytesRead, cmdBytesLeft);

    schedule(dmaWriteWaitEvent, curTick() + totalDiskDelay);
}

void
IdeDisk::doDmaWrite()
{
    if (dmaAborted) {
        DPRINTF(IdeDisk, "DMA Aborted while doing DMA Write\n");
        if (dmaWriteCG)
            delete dmaWriteCG;
        dmaWriteCG = NULL;
        updateState(ACT_CMD_ERROR);
        return;
    }
    if (!dmaWriteCG) {
        // clear out the data buffer
        dmaWriteCG = new ChunkGenerator(curPrd.getBaseAddr(),
                curPrd.getByteCount(), TheISA::PageBytes);
    }
    if (ctrl->dmaPending() || ctrl->drainState() != DrainState::Running) {
        schedule(dmaWriteWaitEvent, curTick() + DMA_BACKOFF_PERIOD);
        DPRINTF(IdeDisk, "doDmaWrite: rescheduling\n");
        return;
    } else if (!dmaWriteCG->done()) {
        assert(dmaWriteCG->complete() < MAX_DMA_SIZE);
        ctrl->dmaWrite(pciToDma(dmaWriteCG->addr()), dmaWriteCG->size(),
                &dmaWriteWaitEvent, dataBuffer + dmaWriteCG->complete());
        DPRINTF(IdeDisk, "doDmaWrite: not done curPrd byte count %d, eot %#x\n",
                curPrd.getByteCount(), curPrd.getEOT());
        dmaWriteBytes += dmaWriteCG->size();
        dmaWriteTxs++;
        if (dmaWriteCG->size() == TheISA::PageBytes)
            dmaWriteFullPages++;
        dmaWriteCG->next();
    } else {
        DPRINTF(IdeDisk, "doDmaWrite: done curPrd byte count %d, eot %#x\n",
                curPrd.getByteCount(), curPrd.getEOT());
        assert(dmaWriteCG->done());
        delete dmaWriteCG;
        dmaWriteCG = NULL;
        dmaWriteDone();
    }
}

void
IdeDisk::dmaWriteDone()
{
    DPRINTF(IdeDisk, "doWriteDone: curPrd byte count %d, eot %#x cmd bytes left:%d\n",
                curPrd.getByteCount(), curPrd.getEOT(), cmdBytesLeft);
    // check for the EOT
    if (curPrd.getEOT()) {
        assert(cmdBytesLeft == 0);
        dmaState = Dma_Idle;
        updateState(ACT_DMA_DONE);
    } else {
        doDmaTransfer();
    }
}

////
// Disk utility routines
///

void
IdeDisk::readDisk(uint32_t sector, uint8_t *data)
{
    uint32_t bytesRead = image->read(data, sector);

    if (bytesRead != SectorSize)
        panic("Can't read from %s. Only %d of %d read. errno=%d\n",
              name(), bytesRead, SectorSize, errno);
}

void
IdeDisk::writeDisk(uint32_t sector, uint8_t *data)
{
    uint32_t bytesWritten = image->write(data, sector);

    if (bytesWritten != SectorSize)
        panic("Can't write to %s. Only %d of %d written. errno=%d\n",
              name(), bytesWritten, SectorSize, errno);
}

////
// Setup and handle commands
////

void
IdeDisk::startDma(const uint32_t &prdTableBase)
{
    if (dmaState != Dma_Start)
        panic("Inconsistent DMA state, should be in Dma_Start!\n");

    if (devState != Transfer_Data_Dma)
        panic("Inconsistent device state for DMA start!\n");

    // PRD base address is given by bits 31:2
    curPrdAddr = pciToDma((Addr)(prdTableBase & ~ULL(0x3)));

    dmaState = Dma_Transfer;

    // schedule dma transfer (doDmaTransfer)
    schedule(dmaTransferEvent, curTick() + 1);
}

void
IdeDisk::abortDma()
{
    if (dmaState == Dma_Idle)
        panic("Inconsistent DMA state, should be Start or Transfer!");

    if (devState != Transfer_Data_Dma && devState != Prepare_Data_Dma)
        panic("Inconsistent device state, should be Transfer or Prepare!\n");

    updateState(ACT_CMD_ERROR);
}

void
IdeDisk::startCommand()
{
    DevAction_t action = ACT_NONE;
    uint32_t size = 0;
    dmaRead = false;

    // Decode commands
    switch (cmdReg.command) {
        // Supported non-data commands
      case WDSF_READ_NATIVE_MAX:
        size = (uint32_t)image->size() - 1;
        cmdReg.sec_num = (size & 0xff);
        cmdReg.cyl_low = ((size & 0xff00) >> 8);
        cmdReg.cyl_high = ((size & 0xff0000) >> 16);
        cmdReg.head = ((size & 0xf000000) >> 24);

        devState = Command_Execution;
        action = ACT_CMD_COMPLETE;
        break;

      case WDCC_RECAL:
      case WDCC_IDP:
      case WDCC_STANDBY_IMMED:
      case WDCC_FLUSHCACHE:
      case WDSF_VERIFY:
      case WDSF_SEEK:
      case SET_FEATURES:
      case WDCC_SETMULTI:
      case WDCC_IDLE:
        devState = Command_Execution;
        action = ACT_CMD_COMPLETE;
        break;

        // Supported PIO data-in commands
      case WDCC_IDENTIFY:
        cmdBytes = cmdBytesLeft = sizeof(struct ataparams);
        devState = Prepare_Data_In;
        action = ACT_DATA_READY;
        break;

      case WDCC_READMULTI:
      case WDCC_READ:
        if (!(cmdReg.drive & DRIVE_LBA_BIT))
            panic("Attempt to perform CHS access, only supports LBA\n");

        if (cmdReg.sec_count == 0)
            cmdBytes = cmdBytesLeft = (256 * SectorSize);
        else
            cmdBytes = cmdBytesLeft = (cmdReg.sec_count * SectorSize);

        curSector = getLBABase();

        /** @todo make this a scheduled event to simulate disk delay */
        devState = Prepare_Data_In;
        action = ACT_DATA_READY;
        break;

        // Supported PIO data-out commands
      case WDCC_WRITEMULTI:
      case WDCC_WRITE:
        if (!(cmdReg.drive & DRIVE_LBA_BIT))
            panic("Attempt to perform CHS access, only supports LBA\n");

        if (cmdReg.sec_count == 0)
            cmdBytes = cmdBytesLeft = (256 * SectorSize);
        else
            cmdBytes = cmdBytesLeft = (cmdReg.sec_count * SectorSize);
        DPRINTF(IdeDisk, "Setting cmdBytesLeft to %d\n", cmdBytesLeft);
        curSector = getLBABase();

        devState = Prepare_Data_Out;
        action = ACT_DATA_READY;
        break;

        // Supported DMA commands
      case WDCC_WRITEDMA:
        dmaRead = true;  // a write to the disk is a DMA read from memory
      case WDCC_READDMA:
        if (!(cmdReg.drive & DRIVE_LBA_BIT))
            panic("Attempt to perform CHS access, only supports LBA\n");

        if (cmdReg.sec_count == 0)
            cmdBytes = cmdBytesLeft = (256 * SectorSize);
        else
            cmdBytes = cmdBytesLeft = (cmdReg.sec_count * SectorSize);
        DPRINTF(IdeDisk, "Setting cmdBytesLeft to %d in readdma\n", cmdBytesLeft);

        curSector = getLBABase();

        devState = Prepare_Data_Dma;
        action = ACT_DMA_READY;
        break;

      default:
        panic("Unsupported ATA command: %#x\n", cmdReg.command);
    }

    if (action != ACT_NONE) {
        // set the BSY bit
        status |= STATUS_BSY_BIT;
        // clear the DRQ bit
        status &= ~STATUS_DRQ_BIT;
        // clear the DF bit
        status &= ~STATUS_DF_BIT;

        updateState(action);
    }
}

////
// Handle setting and clearing interrupts
////

void
IdeDisk::intrPost()
{
    DPRINTF(IdeDisk, "Posting Interrupt\n");
    if (intrPending)
        panic("Attempt to post an interrupt with one pending\n");

    intrPending = true;

    // talk to controller to set interrupt
    if (ctrl) {
        ctrl->intrPost();
    }
}

void
IdeDisk::intrClear()
{
    DPRINTF(IdeDisk, "Clearing Interrupt\n");
    if (!intrPending)
        panic("Attempt to clear a non-pending interrupt\n");

    intrPending = false;

    // talk to controller to clear interrupt
    if (ctrl)
        ctrl->intrClear();
}

////
// Manage the device internal state machine
////

void
IdeDisk::updateState(DevAction_t action)
{
    switch (devState) {
      case Device_Srst:
        if (action == ACT_SRST_SET) {
            // set the BSY bit
            status |= STATUS_BSY_BIT;
        } else if (action == ACT_SRST_CLEAR) {
            // clear the BSY bit
            status &= ~STATUS_BSY_BIT;

            // reset the device state
            reset(devID);
        }
        break;

      case Device_Idle_S:
        if (action == ACT_SELECT_WRITE && !isDEVSelect()) {
            devState = Device_Idle_NS;
        } else if (action == ACT_CMD_WRITE) {
            startCommand();
        }

        break;

      case Device_Idle_SI:
        if (action == ACT_SELECT_WRITE && !isDEVSelect()) {
            devState = Device_Idle_NS;
            intrClear();
        } else if (action == ACT_STAT_READ || isIENSet()) {
            devState = Device_Idle_S;
            intrClear();
        } else if (action == ACT_CMD_WRITE) {
            intrClear();
            startCommand();
        }

        break;

      case Device_Idle_NS:
        if (action == ACT_SELECT_WRITE && isDEVSelect()) {
            if (!isIENSet() && intrPending) {
                devState = Device_Idle_SI;
                intrPost();
            }
            if (isIENSet() || !intrPending) {
                devState = Device_Idle_S;
            }
        }
        break;

      case Command_Execution:
        if (action == ACT_CMD_COMPLETE) {
            // clear the BSY bit
            setComplete();

            if (!isIENSet()) {
                devState = Device_Idle_SI;
                intrPost();
            } else {
                devState = Device_Idle_S;
            }
        }
        break;

      case Prepare_Data_In:
        if (action == ACT_CMD_ERROR) {
            // clear the BSY bit
            setComplete();

            if (!isIENSet()) {
                devState = Device_Idle_SI;
                intrPost();
            } else {
                devState = Device_Idle_S;
            }
        } else if (action == ACT_DATA_READY) {
            // clear the BSY bit
            status &= ~STATUS_BSY_BIT;
            // set the DRQ bit
            status |= STATUS_DRQ_BIT;

            // copy the data into the data buffer
            if (cmdReg.command == WDCC_IDENTIFY) {
                // Reset the drqBytes for this block
                drqBytesLeft = sizeof(struct ataparams);

                memcpy((void *)dataBuffer, (void *)&driveID,
                       sizeof(struct ataparams));
            } else {
                // Reset the drqBytes for this block
                drqBytesLeft = SectorSize;

                readDisk(curSector++, dataBuffer);
            }

            // put the first two bytes into the data register
            memcpy((void *)&cmdReg.data, (void *)dataBuffer,
                   sizeof(uint16_t));

            if (!isIENSet()) {
                devState = Data_Ready_INTRQ_In;
                intrPost();
            } else {
                devState = Transfer_Data_In;
            }
        }
        break;

      case Data_Ready_INTRQ_In:
        if (action == ACT_STAT_READ) {
            devState = Transfer_Data_In;
            intrClear();
        }
        break;

      case Transfer_Data_In:
        if (action == ACT_DATA_READ_BYTE || action == ACT_DATA_READ_SHORT) {
            if (action == ACT_DATA_READ_BYTE) {
                panic("DEBUG: READING DATA ONE BYTE AT A TIME!\n");
            } else {
                drqBytesLeft -= 2;
                cmdBytesLeft -= 2;

                // copy next short into data registers
                if (drqBytesLeft)
                    memcpy((void *)&cmdReg.data,
                           (void *)&dataBuffer[SectorSize - drqBytesLeft],
                           sizeof(uint16_t));
            }

            if (drqBytesLeft == 0) {
                if (cmdBytesLeft == 0) {
                    // Clear the BSY bit
                    setComplete();
                    devState = Device_Idle_S;
                } else {
                    devState = Prepare_Data_In;
                    // set the BSY_BIT
                    status |= STATUS_BSY_BIT;
                    // clear the DRQ_BIT
                    status &= ~STATUS_DRQ_BIT;

                    /** @todo change this to a scheduled event to simulate
                        disk delay */
                    updateState(ACT_DATA_READY);
                }
            }
        }
        break;

      case Prepare_Data_Out:
        if (action == ACT_CMD_ERROR || cmdBytesLeft == 0) {
            // clear the BSY bit
            setComplete();

            if (!isIENSet()) {
                devState = Device_Idle_SI;
                intrPost();
            } else {
                devState = Device_Idle_S;
            }
        } else if (action == ACT_DATA_READY && cmdBytesLeft != 0) {
            // clear the BSY bit
            status &= ~STATUS_BSY_BIT;
            // set the DRQ bit
            status |= STATUS_DRQ_BIT;

            // clear the data buffer to get it ready for writes
            memset(dataBuffer, 0, MAX_DMA_SIZE);

            // reset the drqBytes for this block
            drqBytesLeft = SectorSize;

            if (cmdBytesLeft == cmdBytes || isIENSet()) {
                devState = Transfer_Data_Out;
            } else {
                devState = Data_Ready_INTRQ_Out;
                intrPost();
            }
        }
        break;

      case Data_Ready_INTRQ_Out:
        if (action == ACT_STAT_READ) {
            devState = Transfer_Data_Out;
            intrClear();
        }
        break;

      case Transfer_Data_Out:
        if (action == ACT_DATA_WRITE_BYTE ||
            action == ACT_DATA_WRITE_SHORT) {

            if (action == ACT_DATA_READ_BYTE) {
                panic("DEBUG: WRITING DATA ONE BYTE AT A TIME!\n");
            } else {
                // copy the latest short into the data buffer
                memcpy((void *)&dataBuffer[SectorSize - drqBytesLeft],
                       (void *)&cmdReg.data,
                       sizeof(uint16_t));

                drqBytesLeft -= 2;
                cmdBytesLeft -= 2;
            }

            if (drqBytesLeft == 0) {
                // copy the block to the disk
                writeDisk(curSector++, dataBuffer);

                // set the BSY bit
                status |= STATUS_BSY_BIT;
                // set the seek bit
                status |= STATUS_SEEK_BIT;
                // clear the DRQ bit
                status &= ~STATUS_DRQ_BIT;

                devState = Prepare_Data_Out;

                /** @todo change this to a scheduled event to simulate
                    disk delay */
                updateState(ACT_DATA_READY);
            }
        }
        break;

      case Prepare_Data_Dma:
        if (action == ACT_CMD_ERROR) {
            // clear the BSY bit
            setComplete();

            if (!isIENSet()) {
                devState = Device_Idle_SI;
                intrPost();
            } else {
                devState = Device_Idle_S;
            }
        } else if (action == ACT_DMA_READY) {
            // clear the BSY bit
            status &= ~STATUS_BSY_BIT;
            // set the DRQ bit
            status |= STATUS_DRQ_BIT;

            devState = Transfer_Data_Dma;

            if (dmaState != Dma_Idle)
                panic("Inconsistent DMA state, should be Dma_Idle\n");

            dmaState = Dma_Start;
            // wait for the write to the DMA start bit
        }
        break;

      case Transfer_Data_Dma:
        if (action == ACT_CMD_ERROR) {
            dmaAborted = true;
            devState = Device_Dma_Abort;
        } else if (action == ACT_DMA_DONE) {
            // clear the BSY bit
            setComplete();
            // set the seek bit
            status |= STATUS_SEEK_BIT;
            // clear the controller state for DMA transfer
            ctrl->setDmaComplete(this);

            if (!isIENSet()) {
                devState = Device_Idle_SI;
                intrPost();
            } else {
                devState = Device_Idle_S;
            }
        }
        break;

      case Device_Dma_Abort:
        if (action == ACT_CMD_ERROR) {
            setComplete();
            status |= STATUS_SEEK_BIT;
            ctrl->setDmaComplete(this);
            dmaAborted = false;
            dmaState = Dma_Idle;

            if (!isIENSet()) {
                devState = Device_Idle_SI;
                intrPost();
            } else {
                devState = Device_Idle_S;
            }
        } else {
            DPRINTF(IdeDisk, "Disk still busy aborting previous DMA command\n");
        }
        break;

      default:
        panic("Unknown IDE device state: %#x\n", devState);
    }
}

void
IdeDisk::serialize(CheckpointOut &cp) const
{
    // Check all outstanding events to see if they are scheduled
    // these are all mutually exclusive
    Tick reschedule = 0;
    Events_t event = None;

    int eventCount = 0;

    if (dmaTransferEvent.scheduled()) {
        reschedule = dmaTransferEvent.when();
        event = Transfer;
        eventCount++;
    }
    if (dmaReadWaitEvent.scheduled()) {
        reschedule = dmaReadWaitEvent.when();
        event = ReadWait;
        eventCount++;
    }
    if (dmaWriteWaitEvent.scheduled()) {
        reschedule = dmaWriteWaitEvent.when();
        event = WriteWait;
        eventCount++;
    }
    if (dmaPrdReadEvent.scheduled()) {
        reschedule = dmaPrdReadEvent.when();
        event = PrdRead;
        eventCount++;
    }
    if (dmaReadEvent.scheduled()) {
        reschedule = dmaReadEvent.when();
        event = DmaRead;
        eventCount++;
    }
    if (dmaWriteEvent.scheduled()) {
        reschedule = dmaWriteEvent.when();
        event = DmaWrite;
        eventCount++;
    }

    assert(eventCount <= 1);

    SERIALIZE_SCALAR(reschedule);
    SERIALIZE_ENUM(event);

    // Serialize device registers
    SERIALIZE_SCALAR(cmdReg.data);
    SERIALIZE_SCALAR(cmdReg.sec_count);
    SERIALIZE_SCALAR(cmdReg.sec_num);
    SERIALIZE_SCALAR(cmdReg.cyl_low);
    SERIALIZE_SCALAR(cmdReg.cyl_high);
    SERIALIZE_SCALAR(cmdReg.drive);
    SERIALIZE_SCALAR(cmdReg.command);
    SERIALIZE_SCALAR(status);
    SERIALIZE_SCALAR(nIENBit);
    SERIALIZE_SCALAR(devID);

    // Serialize the PRD related information
    SERIALIZE_SCALAR(curPrd.entry.baseAddr);
    SERIALIZE_SCALAR(curPrd.entry.byteCount);
    SERIALIZE_SCALAR(curPrd.entry.endOfTable);
    SERIALIZE_SCALAR(curPrdAddr);

    /** @todo need to serialized chunk generator stuff!! */
    // Serialize current transfer related information
    SERIALIZE_SCALAR(cmdBytesLeft);
    SERIALIZE_SCALAR(cmdBytes);
    SERIALIZE_SCALAR(drqBytesLeft);
    SERIALIZE_SCALAR(curSector);
    SERIALIZE_SCALAR(dmaRead);
    SERIALIZE_SCALAR(intrPending);
    SERIALIZE_SCALAR(dmaAborted);
    SERIALIZE_ENUM(devState);
    SERIALIZE_ENUM(dmaState);
    SERIALIZE_ARRAY(dataBuffer, MAX_DMA_SIZE);
}

void
IdeDisk::unserialize(CheckpointIn &cp)
{
    // Reschedule events that were outstanding
    // these are all mutually exclusive
    Tick reschedule = 0;
    Events_t event = None;

    UNSERIALIZE_SCALAR(reschedule);
    UNSERIALIZE_ENUM(event);

    switch (event) {
      case None : break;
      case Transfer : schedule(dmaTransferEvent, reschedule); break;
      case ReadWait : schedule(dmaReadWaitEvent, reschedule); break;
      case WriteWait : schedule(dmaWriteWaitEvent, reschedule); break;
      case PrdRead : schedule(dmaPrdReadEvent, reschedule); break;
      case DmaRead : schedule(dmaReadEvent, reschedule); break;
      case DmaWrite : schedule(dmaWriteEvent, reschedule); break;
    }

    // Unserialize device registers
    UNSERIALIZE_SCALAR(cmdReg.data);
    UNSERIALIZE_SCALAR(cmdReg.sec_count);
    UNSERIALIZE_SCALAR(cmdReg.sec_num);
    UNSERIALIZE_SCALAR(cmdReg.cyl_low);
    UNSERIALIZE_SCALAR(cmdReg.cyl_high);
    UNSERIALIZE_SCALAR(cmdReg.drive);
    UNSERIALIZE_SCALAR(cmdReg.command);
    UNSERIALIZE_SCALAR(status);
    UNSERIALIZE_SCALAR(nIENBit);
    UNSERIALIZE_SCALAR(devID);

    // Unserialize the PRD related information
    UNSERIALIZE_SCALAR(curPrd.entry.baseAddr);
    UNSERIALIZE_SCALAR(curPrd.entry.byteCount);
    UNSERIALIZE_SCALAR(curPrd.entry.endOfTable);
    UNSERIALIZE_SCALAR(curPrdAddr);

    /** @todo need to serialized chunk generator stuff!! */
    // Unserialize current transfer related information
    UNSERIALIZE_SCALAR(cmdBytes);
    UNSERIALIZE_SCALAR(cmdBytesLeft);
    UNSERIALIZE_SCALAR(drqBytesLeft);
    UNSERIALIZE_SCALAR(curSector);
    UNSERIALIZE_SCALAR(dmaRead);
    UNSERIALIZE_SCALAR(intrPending);
    UNSERIALIZE_SCALAR(dmaAborted);
    UNSERIALIZE_ENUM(devState);
    UNSERIALIZE_ENUM(dmaState);
    UNSERIALIZE_ARRAY(dataBuffer, MAX_DMA_SIZE);
}

IdeDisk *
IdeDiskParams::create()
{
    return new IdeDisk(this);
}
