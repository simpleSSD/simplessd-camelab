#ifndef __ssdsim_h__
#define __ssdsim_h__

#include "dev/SimpleSSD_types.h"

#include <iostream>
#include <string>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <exception>
#include <fstream>
using namespace std;

#include "dev/GlobalConfig.h"
#include "dev/ConfigReader.h"



#include "dev/Latency.h"
#include "dev/LatencySLC.h"
#include "dev/LatencyMLC.h"
#include "dev/LatencyTLC.h"

#include "dev/Simulator.h"
#include "dev/PALStatistics.h"

#include "dev/PAL2.h"

#include "dev/ftl.hh"



#endif //__ssdsims_h__
