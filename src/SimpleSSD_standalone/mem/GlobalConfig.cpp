#include "mem/GlobalConfig.h"
#include "mem/ConfigReader.h"
//#include "debug/GLOBALCONFIG.hh"



GlobalConfig::GlobalConfig(ConfigReader * cr){


	NANDType = cr->ReadInt32("NANDType",NAND_TLC),
	NumChannel = cr->ReadInt32("NumChannel",1),
	NumPackage = cr->ReadInt32("NumPackage",1),
	NumDie = cr->ReadInt32("NumDie",1),
	NumPlane = cr->ReadInt32("NumPlane",2),
	NumBlock = cr->ReadInt32("NumBlock",1368),
	NumPage = cr->ReadInt32("NumPage",384),
	SizePage = cr->ReadInt32("SizePage",8192),
	DMAMHz =  cr->ReadInt32("DMAMhz",50),
	EnableDMAPreemption = cr->ReadInt32("DMAPreemption", 1),

	FTLOP = cr->ReadFloat("FTLOP", 0.25);
	FTLGCThreshold = cr->ReadFloat("FTLGCThreshold", 0.07); 
	FTLMapN = cr->ReadInt32("FTLMapN", 32);
    FTLMapK = cr->ReadInt32("FTLMapK", 32);

	FTLEraseCycle = cr->ReadInt32("FTLEraseCycle", 100000); 
	SuperblockDegree = cr->ReadInt32("SuperblockDegree", NumChannel*NumPackage*NumDie);
	Warmup = cr->ReadFloat("Warmup", 1.0); 



  OriginalSizes[ADDR_CHANNEL] = NumChannel;
  OriginalSizes[ADDR_PACKAGE] = NumPackage;
  OriginalSizes[ADDR_DIE]     = NumDie;
  OriginalSizes[ADDR_PLANE]   = NumPlane;
  OriginalSizes[ADDR_BLOCK]   = NumBlock;
  OriginalSizes[ADDR_PAGE]    = NumPage;

	int superblock = SuperblockDegree;
	AddrSeq[0] = ADDR_CHANNEL;
	AddrSeq[1] = ADDR_PACKAGE;
	AddrSeq[2] = ADDR_DIE;
	AddrSeq[3] = ADDR_PLANE;
	AddrSeq[4] = ADDR_BLOCK;
	AddrSeq[5] = ADDR_PAGE;
	int offset = 0;
	while (superblock > 1){
		superblock = superblock / OriginalSizes[offset];
		offset++;
	}
	unsigned i;
	for (i = 0; i < offset; i++){
		int tmp = AddrSeq[0];
		for (unsigned j = 1; j < 6 - i; j++){
			AddrSeq[j-1] = AddrSeq[j];
		}
		AddrSeq[6-i-1] = tmp;
	}
  PrintInfo();
}

void GlobalConfig::PrintInfo()
{
    //Use DPRINTF here - ALL of these
    printf("PAL: [ Configuration ]\n");
    printf("PAL: DMAPreemption=%d\n", EnableDMAPreemption);
    printf("PAL: plane count = %llu planes\n", GetTotalNumPlane() );
    printf("PAL: block count = %llu blocks\n", GetTotalNumBlock() );
    printf("PAL: page count = %llu pages\n", GetTotalNumPage() );
    printf("PAL: size = %llu Byte\n", GetTotalSizeSSD() );
    printf("PAL: size = %llu MByte\n", GetTotalSizeSSD()/(MBYTE) );
    printf("PAL: size = %llu GByte\n", GetTotalSizeSSD()/(GBYTE) );
    printf("PAL: AddrSeq:\n");

    printf("PAL: %8s | %8s | %8s | %8s | %8s | %8s\n", ADDR_STRINFO[AddrSeq[0]], ADDR_STRINFO[AddrSeq[1]], ADDR_STRINFO[AddrSeq[2]], ADDR_STRINFO[AddrSeq[3]], ADDR_STRINFO[AddrSeq[4]], ADDR_STRINFO[AddrSeq[5]]);
    

    printf("PAL: %8u | %8u | %8u | %8u | %8u | %8u\n", OriginalSizes[AddrSeq[0]], OriginalSizes[AddrSeq[1]], OriginalSizes[AddrSeq[2]], OriginalSizes[AddrSeq[3]], OriginalSizes[AddrSeq[4]], OriginalSizes[AddrSeq[5]]);

    switch (NANDType)
    {
        default:
        case NAND_TLC: printf("NANDType = TLC (REAL_TLC_PAGE8K)\n"); break;
        case NAND_MLC: printf("NANDType = MLC (ASSUMED_MLC)\n");     break;
        case NAND_SLC: printf("NANDType = SLC (ASSUMED_SLC)\n");     break;
    }
	
    printf("NAND DMA Speed = %u MHz, DMA Pagesize = %u Bytes\n", DMAMHz, SizePage);
}

uint64 GlobalConfig::GetTotalSizeSSD()
{
    return GetTotalNumPage() * (uint64)SizePage;
}

uint64 GlobalConfig::GetTotalNumPage()
{
    return GetTotalNumBlock() * (uint64)NumPage;
}

uint64 GlobalConfig::GetTotalNumBlock()
{
    return GetTotalNumPlane() * (uint64)NumBlock;
}

uint64 GlobalConfig::GetTotalNumDie()
{
    return (uint64)NumChannel * (uint64)NumPackage * (uint64)NumDie;
}

uint64 GlobalConfig::GetTotalNumPlane()
{
    return (uint64)NumChannel * (uint64)NumPackage * (uint64)NumDie * (uint64)NumPlane;
}
